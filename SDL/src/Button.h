#pragma once
#include "LoaderParams.h"
#include "SDLGameObject.h"

//! GameObjects that interact with the mouse. Use callbacks and use animations in a different manner
class Button :
	public SDLGameObject
{
public:

	Button();
	Button( const LoaderParams* loaderParams, void (*callback)());

	virtual void Draw();
	virtual void Update();
	virtual void Clean();
	virtual void Load(const LoaderParams* params);
	

	~Button();

	bool IsEnabled() { return enabled; }

	void SetEnabled(bool my_enabled) { enabled = my_enabled; }

	int GetCallbackID() { return callbackID; }

	void SetCallback(void(*newCallback)()) { callback = newCallback; }

private :

	bool mouseBtReleased;

	bool enabled = true;

	///Function Pointer
	void(*callback) ();

	int callbackID;
};

enum buttonStates
{
	MOUSE_OUT = 0,
	MOUSE_OVER = 1,
	CLICKED = 2,
	DISABLED = 3
};

//! Creates an empty game object of the name of the class. 
class ButtonCreator : public BaseCreator
{
	GameObject* CreateGameObject() const
	{
		return new Button();
	}
};