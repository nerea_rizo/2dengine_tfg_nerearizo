#include "Ship.h"

Ship::Ship()
{
}

Ship::~Ship()
{
}

void Ship::Draw()
{
	SDLGameObject::Draw();
}

void Ship::Update()
{
	velocity += acceleration;
	position += velocity;

	SDLGameObject::Update();
}

void Ship::Load(const LoaderParams * params)
{
	SDLGameObject::Load(params);
	dying = false;
}


void Ship::StartDyingAnimation()
{
	currentFrame = int(((SDL_GetTicks() / (1000 / 10)) % numberOfFrames));
	if (dyingCounter == dyingTime)
	{
		dead = true;
		dying = true;
		textureID = "none";
	}
	dyingCounter++;
}
