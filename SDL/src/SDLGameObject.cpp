#include "SDLGameObject.h"
//Game lo incluyo aqui porque necesito que la clase este declarada antes de llamarlo
#include "Game.h"


SDLGameObject::SDLGameObject(): GameObject()
{
}

SDLGameObject::~SDLGameObject()
{
}

SDLGameObject::SDLGameObject(const LoaderParams* pParams) :
	GameObject(pParams), position(pParams->GetX(), pParams->GetY()), velocity(0,0), acceleration(0,0), degRotation(0)
{

	width = pParams->GetWidth();
	height = pParams->GetHeight();
	textureID = pParams->GetTextureID();
	currentRow = 1;
	currentFrame = 1;
}

SDLGameObject::SDLGameObject(const LoaderParams* pParams, float vx, float vy) :
	GameObject(pParams), position(pParams->GetX(), pParams->GetY()), velocity(vx,vy),acceleration(0,0), degRotation(0)
{

	width = pParams->GetWidth();
	height = pParams->GetHeight();
	textureID = pParams->GetTextureID();
	currentRow = 1;
	currentFrame = 1;
}

SDLGameObject::SDLGameObject(const LoaderParams* pParams, float vx, float vy, float myRadRotation) :
	GameObject(pParams), position(pParams->GetX(), pParams->GetY()), velocity(vx, vy), acceleration(0, 0), degRotation(myRadRotation)
{

	width = pParams->GetWidth();
	height = pParams->GetHeight();
	textureID = pParams->GetTextureID();
	currentRow = 1;
	currentFrame = 1;
}

SDLGameObject::SDLGameObject(const LoaderParams* pParams, float vx, float vy, float ax, float ay) :
	GameObject(pParams), position(pParams->GetX(), pParams->GetY()), velocity(vx, vy), acceleration(ax, ay), degRotation(0)
{

	width = pParams->GetWidth();
	height = pParams->GetHeight();
	textureID = pParams->GetTextureID();
	currentRow = 1;
	currentFrame = 1;
}

SDLGameObject::SDLGameObject(const LoaderParams* pParams, float vx, float vy, float ax, float ay, float myRadRotation) :
	GameObject(pParams), position(pParams->GetX(), pParams->GetY()), velocity(vx, vy), acceleration(ax, ay), degRotation(myRadRotation)
{

	width = pParams->GetWidth();
	height = pParams->GetHeight();
	textureID = pParams->GetTextureID();
	currentRow = 1;
	currentFrame = 1;
}


void SDLGameObject ::Draw()
{
	if (textureID != "GameStateSentinel" || textureID != "none") 
	{
		if (velocity.GetX() >= 0)
		{
			TextureManager::Instance()->DrawFrame(textureID, position.GetX(), position.GetY(),
				width, height, currentRow, currentFrame,
				TheGame::Instance()->GetRenderer(),SDL_FLIP_NONE, degRotation,alpha);
		}
		else
		{
			TextureManager::Instance()->DrawFrame(textureID, position.GetX(), position.GetY(),
				width, height, currentRow, currentFrame,
				TheGame::Instance()->GetRenderer(), SDL_FLIP_HORIZONTAL, degRotation,alpha);
		}
	}
}

void SDLGameObject::Update()
{
	currentFrame = (int)((SDL_GetTicks() / 100) % numberOfFrames);
}

void SDLGameObject::Clean() {}

void SDLGameObject::Load(const LoaderParams * params)
{
	position = Vector2D(params->GetX(), params->GetY());
	velocity = Vector2D(0, 0);
	acceleration = Vector2D(0, 0);
	width = params->GetWidth();
	height = params->GetHeight();
	textureID = params->GetTextureID();
	numberOfFrames = params->GetNumberOfFrames();
	currentRow = 1;
	currentFrame = 0;
	degRotation = 0;
	alpha = 255;
}

void SDLGameObject::Load(const LoaderParams * params, float vx, float vy, float myDegRotation, float ax, float ay)
{
	position = Vector2D(params->GetX(), params->GetY());
	velocity = Vector2D(vx,vy);
	acceleration = Vector2D(ax, ay);
	width = params->GetWidth();
	height = params->GetHeight();
	textureID = params->GetTextureID();
	currentRow = 1;
	currentFrame = 1;
	degRotation = myDegRotation;
}


