#include "Button.h"

Button::Button() : SDLGameObject()
{
}

Button::Button(const LoaderParams* params, void (*my_callback)()) : SDLGameObject(params), callback(my_callback)
{
	//Button starts unselected
	currentFrame = MOUSE_OUT;
}


Button::~Button()
{
}

void Button::Draw() 
{
	//Dependiendo de donde este el raton dibujare un boton u otro
	if (enabled) 
	{
		Vector2D* mousePos = TheInputHandler::Instance()->GetMousePosition();

		//Comprobamos si el raton esta encima del objeto
		if (mousePos->GetX() < (position.GetX() + width) && mousePos->GetX() > position.GetX() &&
			mousePos->GetY() < (position.GetY() + height) && mousePos->GetY() > position.GetY())
		{
			currentFrame = MOUSE_OVER;
			if (TheInputHandler::Instance()->GetMouseButtonState(LEFT) && mouseBtReleased)
			{
				currentFrame = CLICKED;

				SoundManager::Instance()->PlaySound("click",0);

				callback();

				mouseBtReleased = false;

			}
			else if (!TheInputHandler::Instance()->GetMouseButtonState(LEFT))
			{
				mouseBtReleased = true;
				currentFrame = MOUSE_OVER;
			}

		}
		else
		{
			currentFrame = MOUSE_OUT;
		}
	}
	else 
	{
		currentFrame = DISABLED;
	}

	SDLGameObject::Draw();
}

void Button::Update() 
{
	SDLGameObject::Update();
}

void Button::Clean() {
	SDLGameObject::Clean();
}

void Button::Load(const LoaderParams * params)
{
	SDLGameObject::Load(params);
	callbackID = params->GetCallbackID();
	currentFrame = MOUSE_OUT;
}
