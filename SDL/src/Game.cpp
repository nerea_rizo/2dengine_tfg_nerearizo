#include "Game.h"

Game::Game(){}

Game* Game::instance = 0;


Game::~Game()
{
}

Game * Game::Instance()
{
	if (instance == 0)
	{
		instance = new Game();
		return instance;
	}

	return instance;
}


void Game::Update() 
{

	gameStateMachine->Update();
	
}

bool Game::Init(std::string title, int xpos, int ypos, int width, int height, bool fullscreen, bool landscape)
{
	int flags = 0;

	if (fullscreen)
		flags = SDL_WINDOW_RESIZABLE;

	//Initialize SDL
	if (SDL_Init(SDL_INIT_EVERYTHING) == 0)
	{
		std::cout << "SDL Init success \n";

		//If succeded create the window
		if (landscape) {
			gameWindow = SDL_CreateWindow(title.c_str(), xpos, ypos, height, width, flags);
		}
		//portrait mode
		else {
			gameWindow = SDL_CreateWindow(title.c_str(), xpos, ypos, width, height, flags);
		}

		//If succeded creating the window -> create renderer
		if (gameWindow != 0)
		{
			std::cout << "Window creation success \n";

			gameRenderer = SDL_CreateRenderer(gameWindow, -1, 0);

			if (gameRenderer != 0) // renderer Init success
			{
				std::cout << "renderer creation success\n";

				SDL_SetRenderDrawColor(gameRenderer,
					0, 0, 0, 255);
			}
			else
			{
				std::cout << "renderer initialization failed. SDL Error: " << SDL_GetError();
				return false;
			}
		}
		else
		{
			std::cout << "window initialization failed. SDL Error: " << SDL_GetError();
			return false;
		}

	}
	else
	{
		std::cout << "SDL initialization failed. SDL Error: " << SDL_GetError();
		return false;
	}

	std::cout << "SDL initialized. Success!\n";
	TheInputHandler::Instance()->InitialiseJoysticks();

	SDL_RenderSetLogicalSize(gameRenderer, 635, 480);

	Mix_Init(MIX_INIT_FLAC||MIX_INIT_MOD||MIX_INIT_MP3||MIX_INIT_OGG);

	windowLogicalHeight = 635;

	windowLogicalWidth = 480;

	std::cout << std::boolalpha;
	
	gameRunning = true;//Start the main loop


	TheGameObjectFactory::Instance()->RegisterType("Button", new ButtonCreator());
	TheGameObjectFactory::Instance()->RegisterType("PlayerShip", new PlayerShipCreator());
	TheGameObjectFactory::Instance()->RegisterType("BasicEnemyShip", new BasicEnemyCreator());
	TheGameObjectFactory::Instance()->RegisterType("SDLGameObject", new SDLGameObjectCreator());
	TheGameObjectFactory::Instance()->RegisterType("TriShotEnemyShip", new TriShotEnemyCreator());
	TheGameObjectFactory::Instance()->RegisterType("NinjaEnemyShip", new NinjaEnemyCreator());


	SoundManager::Instance()->Load("resources/music/Da_Funky_Rapsta.mp3", "menuMusic", SOUND_MUSIC);
	SoundManager::Instance()->Load("resources/music/Biggie.mp3", "level1Music", SOUND_MUSIC);
	SoundManager::Instance()->Load("resources/music/Night_Theme.mp3", "level2Music", SOUND_MUSIC);

	SoundManager::Instance()->Load("resources/music/switch2.ogg", "click", SOUND_SFX);
	SoundManager::Instance()->Load("resources/music/laser.wav", "shoot", SOUND_SFX);
	SoundManager::Instance()->Load("resources/music/enemyShipDestroyed.wav", "enemyBoom", SOUND_SFX);
	SoundManager::Instance()->Load("resources/music/playerShipDestroyed.wav", "playerBoom", SOUND_SFX);

	gameStateMachine = new GameStateMachine();
	gameStateMachine->PushState (new MainMenuState());

	return true;
}

void Game::Draw() {

	//clear the window to renderer color
	SDL_RenderClear(gameRenderer);

	gameStateMachine->Render();


	//Draw to the screen
	SDL_RenderPresent(gameRenderer);

}

void Game::Quit() {
	std::cout << "cleaning game\n";
	TheInputHandler::Instance()->Clean();
	SDL_DestroyWindow(gameWindow);
	SDL_DestroyRenderer(gameRenderer);
	IMG_Quit();
	//Mix_Quit();
	SDL_Quit();
	gameRunning = false;
}

void Game::HandleEvents()
{
	TheInputHandler::Instance()->Update(gameRunning);
}

