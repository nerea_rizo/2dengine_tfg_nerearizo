#pragma once
#include "SDLGameObject.h"
#include "Factory\GameObjectFactory.h"
//! Demo Player Class with HandleInput()
class Player :
	public SDLGameObject
{

public:

	Player();
	Player(const LoaderParams* params, float vx, float vy);
	Player(const LoaderParams* params, float vx, float vy, float degRotation);

	virtual void Draw();
	virtual void Update();
	virtual void Clean();
	virtual void Load(const LoaderParams* params);

	~Player();

private:
	/*
	* Reads from keyboard, mouse and controller and sets velocity
	*/
	void HandleInput();
};

//! Creates an empty game object of the name of the class. 
class PlayerCreator : public BaseCreator
{
	GameObject* CreateGameObject() const
	{
		return new Player();
	}
};