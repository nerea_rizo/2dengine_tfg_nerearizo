#pragma once

#include <iostream>
#include <map>
#include <SDL_mixer.h>

//using namespace SDLMixer;

enum soundType
{
	SOUND_MUSIC = 0,
	SOUND_SFX = 1
};

//! Loads, stores and reproduces all the sounds of the engine.
class SoundManager
{
public:
	/*
	*Static access point for the SoundManager
	*/
	static SoundManager* Instance()
	{
		if (instance == 0)
		{
			instance = new SoundManager();
			return instance;
		}
		return instance;
	}

	/*
	*Loads a sound or a music into the musicMap or the sfxsMap. If loading fails an error is prompted.
	*/
	bool Load(std::string fileName, std::string id, soundType type);

	/*
	*Plays a sound effect the given number of times.
	*/
	void PlaySound(std::string id, int loop);

	/*
	* Plays a music file the given number of times.
	*/
	void PlayMusic(std::string id, int loop);

	/*
	*Stops all music samples running.
	*/
	void StopMusic();

	/*
	* Stops all sound samples running.
	*/
	void StopSFX();

private:
	static SoundManager * instance;

	std::map<std::string, Mix_Chunk*> sfxsMap;
	std::map<std::string, Mix_Music*> musicMap;

	SoundManager();
	~SoundManager();
};


