#pragma once
#include "EnemyShip.h"
//! Very fast ship that does not shoot
class NinjaEnemyShip :	public EnemyShip
{
public:
	NinjaEnemyShip();
	~NinjaEnemyShip();

	virtual void Load(const LoaderParams* params);
	virtual void Update();
	virtual void Draw();
	virtual void OnCollision();

private:

	int shootingSpeed = 10;
	int framesUntilShot = 0;

	int movementSpeed = 6;
	int dyingTime = 25;
	int health = 1;
	bool dying = false;
	bool dead = false;
};

//! Creates an empty game object of the name of the class. 
class NinjaEnemyCreator :public BaseCreator
{
	GameObject* CreateGameObject() const
	{
		return new NinjaEnemyShip();
	}
};
#include "BulletHandler.h"