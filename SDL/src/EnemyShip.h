#pragma once
#include "Ship.h"

//! Includes health and separates ships from player and bullets
class EnemyShip :	public Ship
{
public:
	EnemyShip();

protected:
	int health;
	~EnemyShip();
};

