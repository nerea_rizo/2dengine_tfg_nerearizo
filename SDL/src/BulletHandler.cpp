#include "BulletHandler.h"

BulletHandler* BulletHandler::instance = 0;

BulletHandler::BulletHandler()
{
}


BulletHandler::~BulletHandler()
{
}

BulletHandler * BulletHandler::Instance()
{

	if (instance == 0)
	{
		instance = new BulletHandler();
		return instance;
	}

	return instance;
}

void BulletHandler::AddPlayerBullet(int x, int y, int width, int height, std::string textureID, int numberOfFrames, Vector2D direction)
{
	PlayerBullet* playerBullet = new PlayerBullet();

	playerBullet->Load(new LoaderParams( x, y, width, height, textureID, 0, numberOfFrames), direction);

	playerBullets.push_back(playerBullet);
}

void BulletHandler::AddEnemyBullet(int x, int y, int width, int height, std::string textureID, int numberOfFrames, Vector2D direction)
{
	EnemyBullet* enemyBullet = new EnemyBullet();

	enemyBullet->Load(new LoaderParams(x, y, width, height, textureID, 0, numberOfFrames), direction);

	enemyBullets.push_back(enemyBullet);
}

void BulletHandler::Clear()
{
	playerBullets.clear();
	enemyBullets.clear();
}

void BulletHandler::Update()
{

	for (int i = 0; i < playerBullets.size(); i++) 
	{
		
		if (playerBullets[i]->GetPosition().GetX() >TheGame::Instance()->GetWindowWidth() + 200 ||
			playerBullets[i]->GetPosition().GetX() < 0 ||
			playerBullets[i]->GetPosition().GetY() < 0 ||
			playerBullets[i]->GetPosition().GetY() > TheGame::Instance()->GetWindowHeight() ||	playerBullets[i]->IsDead())
		{
			delete playerBullets[i];
			playerBullets.erase(playerBullets.begin() + i);
		}
		else
		{

			playerBullets[i]->Update();
		}
	}

	for (int i = 0; i < enemyBullets.size(); i++)
	{

		if (enemyBullets[i]->GetPosition().GetX() >TheGame::Instance()->GetWindowWidth() + 200 ||
			enemyBullets[i]->GetPosition().GetX() < 0 ||
			enemyBullets[i]->GetPosition().GetY() < 0 ||
			enemyBullets[i]->GetPosition().GetY() > TheGame::Instance()->GetWindowHeight() || enemyBullets[i]->IsDead())
		{
			delete enemyBullets[i];
			enemyBullets.erase(enemyBullets.begin() + i);
		}
		else
		{
			enemyBullets[i]->Update();
		}
	}
}

void BulletHandler::Draw()
{
	for (int i = 0; i < playerBullets.size(); i++)
	{
		playerBullets[i]->Draw();
	}

	for (int i = 0; i < enemyBullets.size(); i++)
	{
		enemyBullets[i]->Draw();
	}
}
