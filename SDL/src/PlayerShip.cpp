#include "PlayerShip.h"
PlayerShip::PlayerShip()
{
}


PlayerShip::~PlayerShip()
{
}

void PlayerShip::Load(const LoaderParams* params)
{
	SDLGameObject::Load(params);
	degRotation = 90;
	numberOfFrames = 4;
}

void PlayerShip::Update() {
	if (playState->levelCompleted)
	{
		if (position.GetX() > TheGame::Instance()->GetWindowWidth() + 100) {
			playState->OnFinishLevel();
		}
		else 
		{
			acceleration.SetX(2);
			velocity.SetX(3);
			velocity.SetY(0);
			Animate();

			Ship::Update();
		}
	}
	else 
	{
		if (!dying) 
		{
			velocity.SetX(0);
			velocity.SetY(0);

			HandleInput();
			Animate();

#line 28 "Adding vel and acc to a gameobject"
			velocity += acceleration;
			position += velocity;

			Ship::Update();

			framesUntilShot++;
		}
		else
		{
			currentFrame = int(((SDL_GetTicks() / (100)) % numberOfFrames));
			// if the death animation has completed
			if (dyingCounter == dyingTime)
			{
				// ressurect the player
				RespawnPlayer();
				framesUntilShot = 180;
			}
			dyingCounter++;
		}
	}

	
}

void PlayerShip::HandleInput() {
	//JopystickInput
	if (TheInputHandler::Instance()->GetJoysticksInitialized())
	{
		if (TheInputHandler::Instance()->Xvalue(0, 1) > 0 ||TheInputHandler::Instance()->Xvalue(0, 1) < 0)
		{
			velocity.SetX(1 * TheInputHandler::Instance()->Xvalue(0,1));
		}
		if (TheInputHandler::Instance()->Yvalue(0, 1) > 0 ||TheInputHandler::Instance()->Yvalue(0, 1) < 0)
		{
			velocity.SetY(1 * TheInputHandler::Instance()->Yvalue(0,1));
		}
		if (TheInputHandler::Instance()->Xvalue(0, 2) > 0 ||TheInputHandler::Instance()->Xvalue(0, 2) < 0)
		{
			velocity.SetX(1 * TheInputHandler::Instance()->Xvalue(0,2));
		}
		if (TheInputHandler::Instance()->Yvalue(0, 2) > 0 ||TheInputHandler::Instance()->Yvalue(0, 2) < 0)
		{
			velocity.SetY(1 * TheInputHandler::Instance()->Yvalue(0,2));
		}
		if (TheInputHandler::Instance()->GetJoyButtonState(0, 0))
		{
			Shoot();
		}
		if (TheInputHandler::Instance()->GetJoyButtonState(0, 4))
		{
			degRotation++;
		}
		if (TheInputHandler::Instance()->GetJoyButtonState(0, 5))
		{
			degRotation--;
		}

	}

	//KeyboardInput
	if (TheInputHandler::Instance()->IsKeyDown(SDL_SCANCODE_RIGHT))
	{
		velocity.SetX(2);
	}
	if (TheInputHandler::Instance()->IsKeyDown(SDL_SCANCODE_LEFT))
	{
		velocity.SetX(-2);
	}
	if (TheInputHandler::Instance()->IsKeyDown(SDL_SCANCODE_UP))
	{
		velocity.SetY(-2);
	}
	if (TheInputHandler::Instance()->IsKeyDown(SDL_SCANCODE_DOWN))
	{
		velocity.SetY(2);
	}
	if (TheInputHandler::Instance()->IsKeyDown(SDL_SCANCODE_SPACE))
	{
		Shoot();
	}
	if (TheInputHandler::Instance()->IsKeyDown(SDL_SCANCODE_1))
	{
		degRotation++;
	}
	if (TheInputHandler::Instance()->IsKeyDown(SDL_SCANCODE_2))
	{
		degRotation--;
	}
}

void PlayerShip::Animate()
{
	if(invulnerable)
	{
		if(countedInvulnerableFrames >= invulnerableTime)
		{
			invulnerable = false;

			countedInvulnerableFrames = 0;
			alpha = 255;
		}
		else 
		{
			if (alpha == 0)
			{
				alpha = 255;
			}
			else 
			{
				alpha = 0;
			}
		}
		countedInvulnerableFrames++;
	}
}

void PlayerShip::Shoot()
{

	Vector2D shootingPoint = CollisionManager::RotatePointAroundAnother(centerOfObj,  Vector2D(centerOfObj.GetX(), centerOfObj.GetY() - height /2), degRotation);

	if (framesUntilShot >= shootingSpeed)
	{
		SoundManager::Instance()->PlaySound("shoot",0);
		TheBulletHandler::Instance()->AddPlayerBullet(position.GetX() +shootingPoint.GetX() +10 , position.GetY()+ shootingPoint.GetY() + 15, 11, 11, "bullet", 1, Vector2D(shootingPoint.GetX() / 3,shootingPoint.GetY()/3));
		framesUntilShot = 0;
	}

}

void PlayerShip::OnCollision()
{
	if (!invulnerable)
	{
		if (!deathSoundPlayed)
		{
			SoundManager::Instance()->PlaySound("playerBoom", 0);
			deathSoundPlayed = true;

			textureID = "bigExplosion";
			currentFrame = 0;
			numberOfFrames = 9;
			width = 60;
			height = 60;
			dying = true;
		}
	}
}

void PlayerShip::RespawnPlayer()
{
	playState->EliminateLife();
	position.SetX(100);
	position.SetY(300);

	currentFrame = 0;
	numberOfFrames = 4;
	width = 32;
	height = 43;

	dyingCounter = 0;
	invulnerable = true;
	dead = false;


	textureID = "player";
	deathSoundPlayed = false;
	dying = false;
}
