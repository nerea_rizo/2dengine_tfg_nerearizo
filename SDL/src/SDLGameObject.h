#pragma once
#include <iostream>
#include <SDL.h>
#include "GameObject.h"
#include "InputHandler.h"
#include "Factory\GameObjectFactory.h"
#include "SoundManager.h"

//! Implements Game Object functions. Includes animation and an alpha value
class SDLGameObject :public GameObject
{
public:

	SDLGameObject();
	~SDLGameObject();
	SDLGameObject(const LoaderParams* params);
	SDLGameObject(const LoaderParams* params, float vx,float vy);
	SDLGameObject(const LoaderParams* params, float vx, float vy, float degRotation);
	SDLGameObject(const LoaderParams* params, float vx, float vy, float ax, float ay);
	SDLGameObject(const LoaderParams* pParams, float vx, float vy, float ax, float ay, float degRotation);

	virtual void Draw();
	virtual void Update();
	virtual void Clean();

	virtual void Load(const LoaderParams* params);
	virtual void Load(const LoaderParams* params, float vx, float vy, float degRotation=0, float ax=0, float ay=0);

	virtual void OnCollision() {};

	virtual Vector2D& GetPosition() { return position; }
	virtual Vector2D& GetVelocity() { return velocity; }
	virtual Vector2D& GetAcceleration() { return acceleration; }

	virtual int GetWidth() { return width; }
	virtual int GetHeight() { return height; }
	virtual float GetRotation() { return degRotation; }

	virtual void SetRotation(float newRot) { degRotation=newRot; }
	virtual void SetPosition(Vector2D newPosition) { position = newPosition; };
	virtual void SetAlpha(int alpha) { alpha = alpha; }

	virtual bool IsDead(){ return dead;}
	virtual bool IsDying(){ return dying;}

protected:

	Vector2D position;
	Vector2D velocity;
	Vector2D acceleration;

	int width;
	int height;

	float degRotation;

	int currentRow;
	int currentFrame;
	int numberOfFrames;

	bool dying=false;
	bool dead=false;

	int alpha=255;
	
	std::string textureID;
};


//! Creates an empty game object of the name of the class. 
class SDLGameObjectCreator : public BaseCreator
{
	GameObject* CreateGameObject() const
	{
		return new SDLGameObject();
	}
};

#include "CollisionManager.h"
