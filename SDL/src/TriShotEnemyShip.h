#pragma once
#include "EnemyShip.h"

//! Shoots three bullets with a cone shape. 
class TriShotEnemyShip :	public EnemyShip
{
public:
	TriShotEnemyShip();
	~TriShotEnemyShip();

	virtual void Load(const LoaderParams* params);
	virtual void Update();
	virtual void Draw();
	virtual void OnCollision();

private:

	int movementSpeed = 1;
	int dyingTime = 25;
	int health = 2;
	bool dying = false;
	bool dead = false;

	int shootingSpeed = 240;
	int framesUntilShot = 0;
};

//! Creates an empty game object of the name of the class. 
class TriShotEnemyCreator :public BaseCreator
{
	GameObject* CreateGameObject() const
	{
		return new TriShotEnemyShip();
	}
};
#include "BulletHandler.h"