#pragma once
#include <iostream>

//! Needed to create a GameObject
class LoaderParams
{
public:
	LoaderParams(float my_x, float my_y, int my_width, int my_height, std::string my_textureID) :
		x(my_x), y(my_y),width(my_width), height(my_height),textureID(my_textureID) 
	{
	}

	LoaderParams(int x, int y, int width, int height, std::string textureID, int callbackID = 0, int numberOfFrames= 0) :
		x(x), y(y),	width(width),	height(height),	textureID(textureID),	callbackID(callbackID)	, numberOfFrames(numberOfFrames)
	{
	}


	float GetX() const { return x; }
	float GetY() const { return y; }
	int GetWidth() const { return width; }
	int GetHeight() const { return height; }
	int GetCallbackID() const { return callbackID; }
	int GetNumberOfFrames() const { return numberOfFrames; }
	std::string GetTextureID() const { return textureID; }


	~LoaderParams() {}

private:
	float x;
	float y;

	int width;
	int height;

	int callbackID;
	int numberOfFrames;
	std::string textureID;

};

