#include "SoundManager.h"

SoundManager * SoundManager::instance = 0;

SoundManager::~SoundManager()
{
	Mix_CloseAudio();
}

SoundManager::SoundManager()
{
	//Antes de poder usar el sonido, hay que llamar a Mix_openAudio para preparar el audio
	//los parámetros son : (int frequency, Uint 16 format, int channels, int chunksize)
	Mix_OpenAudio(22050, AUDIO_S16, 2, 4096);
}



bool SoundManager::Load(std::string fileName, std::string id, soundType type)
{
	if (type == SOUND_MUSIC)
	{
		Mix_Music * music = Mix_LoadMUS(fileName.c_str());

		if (music == 0)
		{
			std::cout << "Could not Load music: ERROR - " << Mix_GetError() << std::endl;
			return false;
		}
		musicMap[id] = music;
		return true;
	}
	else if (type == SOUND_SFX)
	{
		Mix_Chunk* chunk = Mix_LoadWAV(fileName.c_str());
		if (chunk == 0)
		{
			std::cout << "Could not Load SFX: ERROR - "	<< Mix_GetError() << std::endl;
			return false;
		}
		sfxsMap[id] = chunk;
		return true;
	}
	return false;

}

void SoundManager::PlayMusic(std::string id, int loop)
{
	Mix_PlayMusic(musicMap[id], loop);
}


void SoundManager::PlaySound(std::string id, int loop)
{
	//-1 -> play sound in any avalailable channel
	Mix_PlayChannel(-1, sfxsMap[id], loop);
}


void SoundManager::StopMusic()
{
	Mix_HaltMusic();
}

void SoundManager::StopSFX()
{
	Mix_Pause(-1);
}


