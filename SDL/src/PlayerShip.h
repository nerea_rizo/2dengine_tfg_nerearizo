#pragma once
#include "Ship.h"
#include "GameStates\PlayState.h"

class PlayState;

//! The player Avatar in the Shoot'em up game
class PlayerShip :public Ship
{
public:
	PlayerShip();
	~PlayerShip();
	virtual void Load(const LoaderParams* params);

	void SetPlayState(PlayState* level) { playState = level;}

	void OnCollision();
	virtual void Update();

private:

	void HandleInput();
	void Animate();
	void Shoot();

	void RespawnPlayer();

	int dyingTime = 20;
	int dyingCounter = 0;
	int invulnerableTime=180;
	int countedInvulnerableFrames=0;
	bool invulnerable=false;

	PlayerShip* player;

	PlayState* playState;

	Vector2D centerOfObj = Vector2D(GetPosition().GetX() + GetWidth() / 2, GetPosition().GetY() + GetHeight() / 2);

	int shootingSpeed = 60;
	int framesUntilShot = 60;
};

//! Creates an empty game object of the name of the class. 
class PlayerShipCreator : public BaseCreator
{
	GameObject* CreateGameObject() const
	{
		return new PlayerShip();
	}
};
#include "BulletHandler.h"