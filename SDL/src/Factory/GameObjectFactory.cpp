#include "GameObjectFactory.h"


GameObjectFactory* GameObjectFactory::instance = 0;

GameObjectFactory* GameObjectFactory::Instance()
{
	if (instance == 0)
	{
		instance = new GameObjectFactory();
		return instance;
	}

	return instance;
}