#pragma once
#include <string>
#include <map>
#include "../GameObject.h"

//! Creates an empty game object of the name of the class. 
class BaseCreator
{
public:
	/// Creates an empty game object of the name of the class. 
	virtual GameObject * CreateGameObject() const = 0;
	virtual ~BaseCreator() {}

};

//! Stores and loads all the GameObjectCreators
class GameObjectFactory
{
public:
	static GameObjectFactory* Instance();

	/**
	* Registers a new GameObject Creator
	*/
	bool RegisterType(std::string typeID, BaseCreator* creator)
	{
		std::map <std::string, BaseCreator*>::iterator iterator = creatorMap.find(typeID);

		//If type alredy registered->do nothing
		if (iterator != creatorMap.end())
		{
			delete creator;
			return false;
		}
		creatorMap[typeID] = creator;

		return true;
	}

	/**
	* Loads a GameObjectCreator and calls its CreateGameObject() function.
	*/
	GameObject* Create(std::string typeID)
	{
		std::map <std::string, BaseCreator*>::iterator iterator = creatorMap.find(typeID);

		//If you arrive to the end, the type is not registered
		if (iterator == creatorMap.end())
		{
			std::cout << "creator type not foun in CreatorMap \n";
			return NULL;
		}

		BaseCreator* creator = (*iterator).second;
		return creator->CreateGameObject();
		

	}

private:
	std::map <std::string, BaseCreator*> creatorMap;

	static GameObjectFactory* instance;
};

typedef GameObjectFactory TheGameObjectFactory;