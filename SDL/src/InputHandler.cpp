#include "InputHandler.h"

InputHandler * InputHandler::instance = 0;

InputHandler::InputHandler()
{
	int i;
	for (i = 0; i <= NUMBER_OF_MOUSE_BUTTONS;i++) {
		mouseButtonStates.push_back(false);
	}
	mousePosition = new Vector2D(0,0);
	keystate = 0;
}


InputHandler * InputHandler::Instance() {

	if (instance == 0) {
		instance = new InputHandler();
	}
	return instance;
}

void InputHandler::Update(bool &gameRunning)
{
	SDL_Event event;
	while (SDL_PollEvent(&event))
	{
		switch (event.type)
		{
		case SDL_QUIT:
			gameRunning = false;
			break;
		case SDL_JOYAXISMOTION:
			OnJoystickAxisMove(event);
			break;
		case SDL_JOYBUTTONDOWN:
			OnJoystickButtonDown(event);
			break;
		case SDL_JOYBUTTONUP:
			OnJoystickButtonUp(event);
			break;
		case SDL_MOUSEMOTION:
			OnMouseMove(event);
			break;
		case SDL_MOUSEBUTTONDOWN:
			OnMouseButtonDown(event);
			break;
		case SDL_MOUSEBUTTONUP:
			OnMouseButtonUp(event);
			break;
		case SDL_KEYDOWN:
			OnKeyDown();
			break;
		case SDL_KEYUP:
			OnKeyUp();
			break;
		default:
			break;
		}
	}
}

void InputHandler::OnMouseButtonDown(SDL_Event& event)
{
	if (event.button.button == SDL_BUTTON_LEFT)
	{
		mouseButtonStates[LEFT] = true;
	}

	if (event.button.button == SDL_BUTTON_MIDDLE)
	{
		mouseButtonStates[MIDDLE] = true;
	}

	if (event.button.button == SDL_BUTTON_RIGHT)
	{
		mouseButtonStates[RIGHT] = true;
	}
}

void InputHandler::OnMouseButtonUp(SDL_Event& event) 
{
	if (event.type == SDL_MOUSEBUTTONUP)
	{
		if (event.button.button == SDL_BUTTON_LEFT)
		{
			mouseButtonStates[LEFT] = false;
		}

		if (event.button.button == SDL_BUTTON_MIDDLE)
		{
			mouseButtonStates[MIDDLE] = false;
		}

		if (event.button.button == SDL_BUTTON_RIGHT)
		{
			mouseButtonStates[RIGHT] = false;
		}
	}
}

void InputHandler::OnMouseButtonsReset()
{
	//Every time we change menus the mouse buttons need to be reset
	for (int i = 0; i < mouseButtonStates.size();i++) 
	{
		mouseButtonStates[i] = false;
	}
}

void InputHandler::OnMouseMove(SDL_Event& event)
{
	if (event.type == SDL_MOUSEMOTION)
	{
		mousePosition->SetX(event.motion.x);
		mousePosition->SetY(event.motion.y);
	}
}

void InputHandler::OnJoystickAxisMove(SDL_Event& event)
{
	if (event.type == SDL_JOYAXISMOTION)
	{
		int whichOne = event.jaxis.which; // get which controller

		//Remember jaxis.axis depends on the controller. PS4 jaxis may not be the same as XBOX 360
		//Left stick moves left or right
		if (event.jaxis.axis == 0)
		{
			if (event.jaxis.value > JOYSTICK_DEAD_ZONE)
			{
				joystickAxisValues[whichOne].first->SetX(1);
			}
			else if (event.jaxis.value < -JOYSTICK_DEAD_ZONE)
			{
				joystickAxisValues[whichOne].first->SetX(-1);
			}
			else
			{
				joystickAxisValues[whichOne].first->SetX(0);
			}
		}

		// left stick move up or down
		if (event.jaxis.axis == 1)
		{
			if (event.jaxis.value > JOYSTICK_DEAD_ZONE)
			{
				joystickAxisValues[whichOne].first->SetY(1);
			}
			else if (event.jaxis.value < -JOYSTICK_DEAD_ZONE)
			{
				joystickAxisValues[whichOne].first->SetY(-1);
			}
			else
			{
				joystickAxisValues[whichOne].first->SetY(0);
			}
		}

		//Right stick moves left or right
		if (event.jaxis.axis == 3)
		{
			if (event.jaxis.value > JOYSTICK_DEAD_ZONE)
			{
				joystickAxisValues[whichOne].first->SetX(1);
			}
			else if (event.jaxis.value < -JOYSTICK_DEAD_ZONE)
			{
				joystickAxisValues[whichOne].first->SetX(-1);
			}
			else
			{
				joystickAxisValues[whichOne].first->SetX(0);
			}
		}

		// left stick move up or down
		if (event.jaxis.axis == 4)
		{
			if (event.jaxis.value > JOYSTICK_DEAD_ZONE)
			{
				joystickAxisValues[whichOne].first->SetY(1);
			}
			else if (event.jaxis.value < -JOYSTICK_DEAD_ZONE)
			{
				joystickAxisValues[whichOne].first->SetY(-1);
			}
			else
			{
				joystickAxisValues[whichOne].first->SetY(0);
			}
		}
	}
}

void InputHandler::OnJoystickButtonDown(SDL_Event& event)
{
	if (event.type == SDL_JOYBUTTONDOWN) {
		int whichOne = event.jaxis.which;

		joyButtonStates[whichOne][event.jbutton.button] = true;
	}
}

void InputHandler::OnJoystickButtonUp(SDL_Event& event)
{
	if (event.type == SDL_JOYBUTTONUP) {

		int whichOne = event.jaxis.which;

		joyButtonStates[whichOne][event.jbutton.button] = false;

	}
}

void InputHandler::OnKeyDown()
{
	keystate = SDL_GetKeyboardState(0);
}

void InputHandler::OnKeyUp()
{
	keystate = SDL_GetKeyboardState(0);
}

void InputHandler::Clean() 
{
	if (joysticksInitialized) 
	{
		unsigned int i ;
		for (i=0;i <SDL_NumJoysticks();i++)
		{
			SDL_JoystickClose(joysticks[i]);
		}
	}
}

void InputHandler::InitialiseJoysticks() {

	if (SDL_WasInit(SDL_INIT_JOYSTICK)==0)
	{
		SDL_InitSubSystem(SDL_INIT_JOYSTICK);
	}
	//Open all the joysticks avalaible
	if (SDL_NumJoysticks() > 0)
	{
		int i;

		for (i = 0; i < SDL_NumJoysticks();i++) 
		{
			SDL_Joystick* joy = SDL_JoystickOpen(i);

			//if joy exists it will have an identifier different of 0
			if (joy) 
			{
				joysticks.push_back(joy);

				//This pair represents the 4*2 axis the controller has.
				joystickAxisValues.push_back(std::make_pair(new Vector2D(0,0),new Vector2D(0,0)));

				std::vector <bool> tempButtons;

				int j;
				for (j = 0; j < SDL_JoystickNumButtons(joy);j++) 
				{
					//Initialize all the buttons of the controller to false
					tempButtons.push_back(false);
				}

				joyButtonStates.push_back(tempButtons);
			}
			else
			{
				std::cout << SDL_GetError();
			}
		}

	// -1\SDL_QUERY disable
	//	0\SDL_IGNORE
	//	1\SDL ENABLE

	SDL_JoystickEventState(SDL_ENABLE);
	joysticksInitialized = true;

	std::cout << "Initialised " << joysticks.size() << "joystick(s) " << std::endl;
	}
	else
	{
		joysticksInitialized = false;
	}

}

int InputHandler::Xvalue(int joy, int stick)
{
	float value = 0;

	if (joystickAxisValues.size() > 0)
	{
		//Left Stick
		if (stick == 1)
		{
			value = joystickAxisValues[joy].first->GetX();
		}
		else if (stick == 2)
		{
			value = joystickAxisValues[joy].second->GetX();
		}
	}
	return value;
}

int InputHandler::Yvalue(int joy, int stick)
{
	float value = 0;

	if (joystickAxisValues.size() > 0)
	{
		//Left Stick
		if (stick == 1)
		{
			value = joystickAxisValues[joy].first->GetY();
		}
		else if (stick == 2)
		{
			value = joystickAxisValues[joy].second->GetY();
		}
	}
	return value;
}

bool InputHandler::IsKeyDown(SDL_Scancode key)
{

	if (keystate != 0)
	{
		if (keystate[key] == 1)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	return false;
}

InputHandler::~InputHandler()
{

}

