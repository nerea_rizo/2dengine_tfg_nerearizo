#pragma once
#include <iostream>
#include <map>
#include <SDL.h>
#include <SDL_image.h>

//! Draws textures on the screen and stores the textureMap
class TextureManager
{
private:

	TextureManager();

	static TextureManager* instance;

public:

	~TextureManager();
	/**
	*The Texture Manager singleton access point.
	*/
	static TextureManager* Instance();

	/**
	*In charge of creating and saving the texture in the texture map. 
	*In order to do so, it creates a SDL_Surface (run by CPU) from the sent file. If this succeeds,
	*the surface is then converted to a texture and the space of the surface is freed.
	*If everything run correctly, the texture is saved in the textureMap variable. 
	*The key of the texture is the string id passed in the beginning of the function. 
	*/
	bool Load(std::string filename, std::string id, SDL_Renderer* renderer);

	/**
	*Draws a sprite on the renderer in the position given.
	*/
	void Draw(std::string id, int x, int y, int width, int height, SDL_Renderer* renderer, SDL_RendererFlip = SDL_FLIP_NONE, double rotation=0);

	/**
	*Draws a frame from a spritesheet in the position given. 
	*DrawFrame() is intended for animated sprites and uses the current frame passed by the game object to draw what is intended.
	*/
	void DrawFrame(std::string id, int x, int y, int width, int height, int currentRow, int currentFrame, SDL_Renderer*	pRenderer,	SDL_RendererFlip flip = SDL_FLIP_NONE, double rotation = 0, int alpha =255);

	/**
	*Erases the texture associated to the string. 
	*/
	void ClearFromTextureMap(std::string);

	std::map<std::string, SDL_Texture*> textureMap;
};

typedef TextureManager TheTextureManager;


