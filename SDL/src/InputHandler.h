#pragma once
#include "SDL.h"
#include <vector>
#include <iostream>
#include "Vector2D.h"
///To avoid light taps being registered
const int JOYSTICK_DEAD_ZONE = 8000;
const int NUMBER_OF_MOUSE_BUTTONS = 3;

//! Processes any kind of external event that the engines receive
class InputHandler
{

private:
	InputHandler();
	~InputHandler();


	static InputHandler* instance;

	bool joysticksInitialized;

	std::vector<SDL_Joystick*> joysticks;

	std::vector<std::pair<Vector2D*, Vector2D*>> joystickAxisValues;

	std::vector<std::vector<bool>> joyButtonStates;

	std::vector<bool> mouseButtonStates;

	Vector2D* mousePosition;

	const Uint8* keystate;


	// handle keyboard events
	/*
	*Stores the current state of all the keyboard. 
	*/
	void OnKeyDown();
	/*
	*Stores the current state of all the keyboard. The function is repeated for the sake of code readability.
	*/
	void OnKeyUp();

	// handle mouse events
	/*
	*Updates the mouse position.
	*/
	void OnMouseMove(SDL_Event& event);

	/*
	*Updates the states of the mouse buttons vector setting the pressed button value to true.
	*/
	void OnMouseButtonDown(SDL_Event& event);

	/*
	*Updates the states of the mouse buttons vector if a mouse button has been released.
	*/
	void OnMouseButtonUp(SDL_Event& event);

	// handle joysticks events
	/*
	*Checks and updates both sticks of a controller. If an axis is being used its value is updated to 1 or -1 (digital input).
	*/
	void OnJoystickAxisMove(SDL_Event& event);

	/*
	*Updates the states of the controllerís button vector setting the pressed button value to true.
	*/
	void OnJoystickButtonDown(SDL_Event& event);

	/*
	*Updates the states of the controllerís button vector if a button has been released.
	*/
	void OnJoystickButtonUp(SDL_Event& event);

public:

	/*
	*The InputHandler instance access point.
	*/
	static InputHandler* Instance();

	/*
	*Input Handler is in charge of handling all the interruption events such as closing the application or receiving input. 
	*This function checks for events every frame and calls a function of the class accordingly. 
	*/
	void Update(bool & gameRunning);

	/*
	*If any controller or joystick has been initialized this function closes it 
	*/
	void Clean();

	/*
	*Firstly, it initialises the SDL_SubSystem associated if it is not running yet. It then searches for every joystick avalaible (connected to the PC in that moment) and opens it. 
	*After that it adds a pair representing the 4*2 axis that a controller has to a vector called joystickAxisValues. 
	*It does the same for every button avalaible in the controller but this time pushing booleans to a vector called tempButtons which will then be pushed to a vector called joyButtonStates. 
	*This way we know that the input of the controller that entered started in number x is located in the x position of both vectors. 
	*For example, the third connected controller will have its input updates in the third position of those vectors.
	*/
	void InitialiseJoysticks();

	/*
	*Every time we change of scene the mouse buttons need to be reset. This function sets all the mouse buttons values to false.
	*/
	void OnMouseButtonsReset();

	bool GetJoysticksInitialized() {return joysticksInitialized;}

	bool GetJoyButtonState(int joy, int buttonNumber){return joyButtonStates[joy][buttonNumber];}

	bool GetMouseButtonState(int buttonNumber) { return mouseButtonStates[buttonNumber];}

	Vector2D* GetMousePosition() { return mousePosition; }

	bool IsKeyDown(SDL_Scancode key);

	/*
	*Checks the x value on a stick of a specific controller. 
	*/
	int Xvalue(int joy, int stick);

	/*
	*Checks the y value on a stick of a specific controller.
	*/
	int Yvalue(int joy, int stick);


};

enum mouseButtons {
	LEFT = 0,
	MIDDLE = 1,
	RIGHT=2
};

typedef InputHandler TheInputHandler;

/*
XBOX 360 GAMEPAD SETTINGS

Eje x: Joystick izq ejex
eje y: Joystick izq ejey
Eje Z negativo (analog triger): R2/RT
Eje Z positivo (analog triger): L2/LT
Rotacion X: Joystick der ejex
Rotacion y: Joystick der ejex
1:A
2:B
3:X
4:Y
5:L1/LB
6:R1/RB
7:Back
8:Start
9:Joystick izquierdo pulsado
10:Joystick derecho pulsado
POV: Cruceta (8 sentidos)
*/

