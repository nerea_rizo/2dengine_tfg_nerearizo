#pragma once
#include <math.h>

//! Utility class with all the operators needed to work with 2D vectors
class Vector2D
{
public:
	Vector2D(float temp_x, float temp_y) : x{ temp_x }, y{ temp_y } {}
	Vector2D() : x{ 0 }, y{ 0 } {}

	float GetX() { return x; }
	float GetY() { return y; }

	void SetX(float temp_x) { x = temp_x; }
	void SetY(float temp_y) { y = temp_y; }

	float length() { return sqrt( x * x + y * y); }

	////////////////////////////////////////////
	//Why only  + and - are friend functions?///
	////////////////////////////////////////////

	Vector2D operator+(const Vector2D& v2) const
	{
		return Vector2D(x + v2.x, y + v2.y);
	}

	friend Vector2D& operator+=(Vector2D& v1, const Vector2D& v2)
	{
		v1.x += v2.x;
		v1.y += v2.y;
		return v1;
	}

	Vector2D operator*(float scalar)
	{
		return Vector2D(x * scalar, y * scalar);
	}

	Vector2D& operator*=(float scalar)
	{
		x *= scalar;
		y *= scalar;
		return *this;
	}

	Vector2D operator-(const Vector2D& v2) const
	{
		return Vector2D(x - v2.x, y - v2.y);
	}

	friend Vector2D& operator-=(Vector2D& v1, const Vector2D& v2)
	{
		v1.x -= v2.x;
		v1.y -= v2.y;
		return v1;
	}

	Vector2D operator/(float scalar)
	{
		return Vector2D(x / scalar, y / scalar);
	}

	Vector2D& operator/=(float scalar)
	{
		x /= scalar;
		y /= scalar;
		return *this;
	}

	friend float dotProduct(const Vector2D& v1, const Vector2D& v2)
	{
		return v1.x * v2.x  + v1.y * v2.y;
	}

	void normalize()
	{
		float l = length();
		if (l > 0) // we never want to attempt to divide by 0
		{
			// *= is the operator, not a pointer. Don't get mad
			(*this) *= 1 / l;
		}
	}

	Vector2D rightHandNormal() {return Vector2D(-this->GetY(), this->GetX());}

	Vector2D leftHandNormal () {return Vector2D(this->GetY(), -this->GetX());}

private:
	float x;
	float y;
};

