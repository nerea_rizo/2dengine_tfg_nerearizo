#pragma once
#include "EnemyShip.h"
//! Weak basic ship
class BasicEnemyShip :	public EnemyShip
{
public:
	BasicEnemyShip();
	~BasicEnemyShip();

	virtual void Load(const LoaderParams* params);
	virtual void Update();
	virtual void Draw();
	virtual void OnCollision();


private:
	//std::string myType = "BasicEnemyShip";

	int shootingSpeed = 90;
	int framesUntilShot = 35;

	int movementSpeed = 2;
	int dyingTime = 25;
	int health = 1;
	bool dying = false;
	bool dead = false;
};

//! Creates an empty game object of the name of the class. 
class BasicEnemyCreator :public BaseCreator
{
	GameObject* CreateGameObject() const
	{
		return new BasicEnemyShip();
	}
};
#include "BulletHandler.h"