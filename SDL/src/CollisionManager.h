#pragma once
#include "SDL.h"
#include "Vector2D.h"
#include "SDLGameObject.h"
#include <utility>
#include <array>
#include <cmath>


#define NUM_OF_VERTEX_IN_A_SQUARE 4
#define VECTOR2D_ARRAY std::array<Vector2D, NUM_OF_VERTEX_IN_A_SQUARE>

//! Static class that stores the Collision methods
class CollisionManager
{
public:
	///Buffer that is subsracted to the SDL_Rect for the boundingBox() method
	const static int buffer = 4;
	
	/**
	*The bounding box collision check. It creates an AABB.
	*/
	static bool BoundingBox(const SDL_Rect* First,const SDL_Rect* Second);

	/**
	* The Separating Axis Theorem (SAT) collision test. It creates and OBB. Returns a boolean.
	*/
	static bool SeparatingAxisTheorem(GameObject * first, GameObject * second);

	/**
	* The Separating Axis Theorem(SAT) collision test. It creates and OBB. Returns a pair containing the Minimum Translation Vector (MTV) (needs tuning).
	*/
	static pair<Vector2D, float> SeparatingAxisTheoremWithMTV(GameObject*  First, GameObject*  Second);

	/**
	* Calculates the right hand normal of every edge of a polygon which are needed for the SAT test.
	*/
	static VECTOR2D_ARRAY CalculateSeparatingAxes(GameObject*  obj);

	/**
	* Calculates the right hand normal of every edge of a polygon which are needed for the SAT test.
	*/
	static VECTOR2D_ARRAY CalculateSeparatingAxes(Vector2D topLeft, Vector2D topRight, Vector2D bottomLeft, Vector2D bottomRight);

	/**
	* Calculates the position of the vertex of a rotated GameObject.
	*/
	static VECTOR2D_ARRAY CalculateRotatedPointsOfACube(GameObject*  obj);

	/**
	* Translates a vertex to the origin of the scene, rotates it by the value given and returns the final position after adding the base of rotation again. 
	*/
	static Vector2D RotatePointAroundAnother(Vector2D baseOfRotation, Vector2D pointToBeMoved, float rotation);

	/**
	* Projects a vertex list on an axis and returns the biggest minmaxed one dimensional projection as an interval stored in a Vector2D. This was done to ease notation without having to manage a new class.
	*/
	static Vector2D ProjectOnAxis(Vector2D Axis, VECTOR2D_ARRAY vertexList);

	/**
	* Given two intervals, determines if they are overlapping.
	*/
	static bool AreOverlapping(Vector2D firstProjection, Vector2D secondProjection);

	/**
	*Checks if the Intervals are Overlapping. If so, returns the minimum distance that the GameObjects are overlapping.
	*/
	static float GetOverlap(Vector2D firstProjection, Vector2D secondProjection);

	CollisionManager();
	~CollisionManager();


	static bool DrawSeparatingAxisTheorem(GameObject * first, GameObject * second, SDL_Renderer* renderer);

	static bool DrawBoundingBox(const SDL_Rect* First, const SDL_Rect* Second, SDL_Renderer * renderer);


	const static pair <Vector2D, float> collisionFailure;

	enum SeparatingAxes {
		TOP_EDGE=0,
		RIGHT_EDGE=1,
		BOTTOM_EDGE=2,
		LEFT_EDGE=3
	};

	enum RotatedPoints {
		TOP_LEFT = 0,
		TOP_RIGHT= 1,
		BOTTOM_LEFT = 2,
		BOTTOM_RIGHT= 3
	};
};

