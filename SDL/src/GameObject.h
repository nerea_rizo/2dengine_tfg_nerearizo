#pragma once
using namespace std;

#include "LoaderParams.h"
#include "TextureManager.h"
#include "Vector2D.h"

#define PI 3.1415926535897932384626


//! Virtual GameObject implementation. Creates the base for all the inheritance

class GameObject
{
public:
	/**
	*Calls the Draw() or DrawFrame() function of the texture manager
	*/
	virtual void Draw()=0;

	/**
	*Updates the Game Object values
	*/
	virtual void Update()=0;
	
	/**
	*Deletes everything related to the GameObject
	*/
	virtual void Clean()=0;

	/**
	*Function to be called when a collision is detected
	*/
	virtual void OnCollision() = 0;

	/**
	*Game Objects are normally created empty and then all the data is loaded from the XML. It can be called manually too if the LoaderParams is created as a new
	*/
	virtual void Load(const LoaderParams* params) = 0;

	virtual Vector2D& GetPosition() { return position; }
	virtual Vector2D& GetVelocity() { return velocity; }
	virtual Vector2D& GetAcceleration() { return acceleration; }

	virtual int GetWidth() { return width; }
	virtual int GetHeight() { return height; }

	///Rotation is specified in degrees. 
	virtual float GetRotation() { return degRotation; }

	virtual void SetRotation(float newRot) { degRotation=newRot; }
	virtual void SetPosition(Vector2D newPosition) { position = newPosition; }


	virtual void SetAlpha(int alpha) { alpha = alpha; }

	virtual bool IsDead() { return dead; }
	virtual bool IsDying() { return dying; }

protected:

	Vector2D position;
	Vector2D velocity;
	Vector2D acceleration;

	int width;
	int height;
	float degRotation;

	bool dying = false;
	bool dead = false;

	GameObject() {}
	GameObject(const LoaderParams* params) {}
	
	virtual ~GameObject() {}

};

