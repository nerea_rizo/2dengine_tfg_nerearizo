#include "BasicEnemyShip.h"



BasicEnemyShip::BasicEnemyShip()
{
}


BasicEnemyShip::~BasicEnemyShip()
{
}

void BasicEnemyShip::Load(const LoaderParams * params)
{
	Ship::Load(params);
	degRotation = -90;
	velocity.SetX(-movementSpeed);
	numberOfFrames = 4;
}

void BasicEnemyShip::Update()
{
	if (dead==false)
	{
		if (dying)
		{
			velocity.SetX(0);
			velocity.SetY(0);
			StartDyingAnimation();
		}
		else 
		{
			Ship::Update();

			if (framesUntilShot >= shootingSpeed)
			{
				SoundManager::Instance()->PlaySound("shoot", 0);
				TheBulletHandler::Instance()->AddEnemyBullet(position.GetX() - width / 2, position.GetY() + height / 2 - 5, 11, 11, "bullet", 1, Vector2D(-5, 0));
				framesUntilShot = 0;
			}
			else
			{
				framesUntilShot++;
			}

			if (position.GetX() < -width - 5)
				position.SetX(TheGame::Instance()->GetWindowWidth() + 160);
		}
	}
}

void BasicEnemyShip::Draw()
{
	Ship::Draw();
}

void BasicEnemyShip::OnCollision()
{
	health--;

	if (health<=0)
	{
		if (!deathSoundPlayed)
		{
			SoundManager::Instance()->PlaySound("enemyBoom", 0);
			deathSoundPlayed = true;

			textureID = "explosion";
			currentFrame = 0;
			numberOfFrames = 9;
			width = 40;
			height = 40;
			dying = true;
		}
	}
}
