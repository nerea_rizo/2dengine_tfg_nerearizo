#include "ShipCollisions.h"



ShipCollisions::ShipCollisions()
{
}


ShipCollisions::~ShipCollisions()
{
}

void ShipCollisions::CheckPlayerEnemyBulletCollision(PlayerShip * player)
{
	for (int i = 0; i < TheBulletHandler::Instance()->GetEnemyBullets().size(); i++)
	{
		EnemyBullet* tempBullet = TheBulletHandler::Instance()->GetEnemyBullets()[i];

		if (CollisionManager::SeparatingAxisTheorem(player, tempBullet))
		{
			if (!player->IsDying() && !tempBullet->IsDying())
			{

				tempBullet->OnCollision();
				player->OnCollision();
			}
		}
	}
}

void ShipCollisions::CheckPlayerEnemyCollision(PlayerShip* player, const std::vector<GameObject*>& sceneObjects)
{
	for (int i = 0; i< sceneObjects.size(); i++)
	{

		if (CollisionManager::SeparatingAxisTheorem(player, sceneObjects[i]))
		{
			if (!player->IsDying() && !sceneObjects[i]->IsDying())
			{
				player->OnCollision();
			}
		}
	}
}

void ShipCollisions::CheckEnemyPlayerBulletCollision(const std::vector<GameObject*>& sceneObjects)
{
	std::vector<PlayerBullet*> tempBullets = TheBulletHandler::Instance()->GetPlayerBullets();
	for (int i = 0; i < sceneObjects.size(); i++) 
	{
		for (int j = 0; j < tempBullets.size(); j++)
		{

			if (CollisionManager::SeparatingAxisTheorem(sceneObjects[i], tempBullets[j]))
			{
				if (!sceneObjects[i]->IsDying() && !tempBullets[j]->IsDying())
				{
					sceneObjects[i]->OnCollision();
					tempBullets[j]->OnCollision();
				}
			}
		}
	}
}
