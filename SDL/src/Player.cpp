#include "Player.h"

Player::Player():SDLGameObject()
{
}

Player::Player(const LoaderParams* pParams, float vx, float vy) : SDLGameObject(pParams, vx,vy)
{

}

Player::Player(const LoaderParams* pParams, float vx, float vy, float degRotation) : SDLGameObject (pParams, vx, vy, degRotation)
{

}


void Player::Draw()
{
	SDLGameObject::Draw(); // we now use SDLGameObject
}

void Player::Clean()
{

}

void Player::Load(const LoaderParams* params)
{
	SDLGameObject::Load(params);
	degRotation = 90;
}

void Player::Update() {
	velocity.SetX(0);
	velocity.SetY(0);

	HandleInput();

#line 28 "Adding vel and acc to a gameobject"
	velocity += acceleration;
	position += velocity;

	SDLGameObject::Update();
}


Player::~Player()
{
}

void Player::HandleInput() {
	//JopystickInput
	if (TheInputHandler::Instance()->GetJoysticksInitialized())
	{
		if (TheInputHandler::Instance()->Xvalue(0, 1) > 0 || TheInputHandler::Instance()->Xvalue(0, 1) < 0)
		{
			velocity.SetX(1 * TheInputHandler::Instance()->Xvalue(0, 1));
		}
		if (TheInputHandler::Instance()->Yvalue(0, 1) > 0 || TheInputHandler::Instance()->Yvalue(0, 1) < 0)
		{
			velocity.SetY(1 * TheInputHandler::Instance()->Yvalue(0, 1));
		}
		if (TheInputHandler::Instance()->Xvalue(0, 2) > 0 || TheInputHandler::Instance()->Xvalue(0, 2) < 0)
		{
			velocity.SetX(1 * TheInputHandler::Instance()->Xvalue(0, 2));
		}
		if (TheInputHandler::Instance()->Yvalue(0, 2) > 0 || TheInputHandler::Instance()->Yvalue(0, 2) < 0)
		{
			velocity.SetY(1 * TheInputHandler::Instance()->Yvalue(0, 2));
		}
		if (TheInputHandler::Instance()->GetJoyButtonState(0, 3)) {
			velocity.SetX(1);
		}
		if (TheInputHandler::Instance()->GetMouseButtonState(LEFT))
		{
			velocity.SetX(1);
		}
	}

	//KeyboardInput
	if (TheInputHandler::Instance()->IsKeyDown(SDL_SCANCODE_RIGHT))
	{
		velocity.SetX(2);
	}
	if (TheInputHandler::Instance()->IsKeyDown(SDL_SCANCODE_LEFT))
	{
		velocity.SetX(-2);
	}
	if (TheInputHandler::Instance()->IsKeyDown(SDL_SCANCODE_UP))
	{
		velocity.SetY(-2);
	}
	if (TheInputHandler::Instance()->IsKeyDown(SDL_SCANCODE_DOWN))
	{
		velocity.SetY(2);
	}
	if (TheInputHandler::Instance()->IsKeyDown(SDL_SCANCODE_1))
	{
		degRotation++;
	}
	if (TheInputHandler::Instance()->IsKeyDown(SDL_SCANCODE_2))
	{
		degRotation--;
	}
}
