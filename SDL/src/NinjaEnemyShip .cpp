#include "NinjaEnemyShip.h"



NinjaEnemyShip::NinjaEnemyShip()
{
}


NinjaEnemyShip::~NinjaEnemyShip()
{
}

void NinjaEnemyShip::Load(const LoaderParams * params)
{
	Ship::Load(params);
	degRotation = -90;
	velocity.SetX(-movementSpeed);
}

void NinjaEnemyShip::Update()
{
	if (dead==false)
	{
		if (dying)
		{
			velocity.SetX(0);
			velocity.SetY(0);
			StartDyingAnimation();
		}
		else 
		{
			//Rotation
			if (shootingSpeed <= framesUntilShot)
			{
				framesUntilShot = 0;
				degRotation += 45;
			}
			else
			{
				framesUntilShot++;
			}

			Ship::Update();

			if (position.GetX() < -width - 5)
				position.SetX(TheGame::Instance()->GetWindowWidth() + 160);

		}
	}
}

void NinjaEnemyShip::Draw()
{
	Ship::Draw();
}

void NinjaEnemyShip::OnCollision()
{
	health--;

	if (health<=0)
	{
		if (!deathSoundPlayed)
		{
			SoundManager::Instance()->PlaySound("enemyBoom", 0);
			deathSoundPlayed = true;

			textureID = "explosion";
			currentFrame = 0;
			numberOfFrames = 9;
			width = 40;
			height = 40;
			dying = true;
		}
	}
}
