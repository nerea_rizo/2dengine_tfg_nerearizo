#pragma once
#include "CollisionManager.h"
#include "BulletHandler.h"

class PlayerShip;

//! Checks the collision between all the gameObjects of the Shoot em up
class ShipCollisions
{
public:
	ShipCollisions();
	~ShipCollisions();
	
	void CheckPlayerEnemyBulletCollision(PlayerShip* player);

	void CheckPlayerEnemyCollision(PlayerShip* player, const std::vector<GameObject*> &objects);

	void CheckEnemyPlayerBulletCollision(const	std::vector<GameObject*> &objects);
};
#include "PlayerShip.h"
