#pragma once

#ifndef __Game__
#define __Game__

#include <SDL.h>
#include <SDL_image.h>
#include <iostream>
#include <vector>
#include "TextureManager.h"
#include "InputHandler.h"
#include "BasicEnemyShip.h"
#include "TriShotEnemyShip.h"
#include "NinjaEnemyShip.h"


//GameStates
#include "GameStates\GameStateMachine.h"
#include "GameStates\MainMenuState.h"
#include "GameStates\PlayState.h"

//! Creates and runs the engine
class Game
{
private:
	static Game* instance;

	SDL_Window * gameWindow;
	SDL_Renderer * gameRenderer;
	GameStateMachine * gameStateMachine;
	bool gameRunning;

	SDLGameObject* play;

	///The logical width of the engine. Currently hardcoded as 635.
	int windowLogicalWidth;

	///The logical height of the engine. Currently hardcoded as 480. 
	int windowLogicalHeight;

	Game();

public:
	~Game();

	/**
	* The Game class is a singleton to ensure that only one instance of it is created during runtime and to facilitate the renderer in case some visual debugging is needed. 
	*/
	static Game* Instance();

	/**
	*Init is in charge of initializing everything that is needed before the game starts. It begins initializing SDL, then the window and the renderer.
	*If anything happens an error will be output and the program will stop. 
	*It then initialises all the connected Joysticks, loads the default texture, starts the GameStateMachine and loads all the factories of the GameObjects classes that need to be read from the XML.
	*/
	bool Init(std::string title, int xpos, int ypos, int width, int height, bool fullscreen, bool landscape);

	/**
	*Calls the Draw() method of the current state in the GameStateMachine.
	*/
	void Draw();

	/**
	*Calls the Update() method of the current state in the GameStateMachine.
	*/
	void Update() ;

	/**
	*Calls the Update() method of the Input Handler which is in charge of external events such as input or closing the window.
	*/
	void HandleEvents();

	/**
	*Stops the game instances, cleans the InputHandler and destroys anything that the game has initialized
	*/
	void Quit();

	GameStateMachine* GetGameStateMachine() { return gameStateMachine; }
	
	bool Running() { return gameRunning; }

	SDL_Renderer* GetRenderer() const { return gameRenderer; }

	int GetWindowWidth() { return windowLogicalWidth; }

	int GetWindowHeight() { return windowLogicalHeight; }
};

typedef Game TheGame;
#endif /* defined(__Game__) */