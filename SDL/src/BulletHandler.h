#pragma once
#include "Ship.h"
#include "Game.h"
#include <vector>
//! Bullets from the PlayerShip
class PlayerBullet : public Ship
{
public:
	PlayerBullet() :Ship() {}
	virtual ~PlayerBullet() {}

	virtual void Load(const LoaderParams* params, Vector2D direction)
	{
		Ship::Load(params);
		velocity = direction;
		dead = false;
		dying = false;
	}

	virtual void OnCollision() { dead = true; }

	virtual void Draw() { Ship::Draw(); }

	virtual void Clean() { Ship::Clean(); }

	virtual void Update() { Ship::Update(); }

private:

};

//! Bullets from the enemy ships
class EnemyBullet : public Ship
{
public:
	EnemyBullet() :Ship() {}
	virtual ~EnemyBullet() {}

	virtual void Load(const LoaderParams* params, Vector2D direction)
	{
		Ship::Load(params);
		velocity = direction;
		dead = false;
		dying = false;
	}

	virtual void OnCollision() { dead = true; }

	virtual void Draw() { Ship::Draw(); }

	virtual void Clean() { Ship::Clean(); }

	virtual void Update() { Ship::Update(); }

private:

};

//! Adds, manages, updates and draws the bullets of the shoot'em up
class BulletHandler
{
public:
	BulletHandler();
	~BulletHandler();

	static BulletHandler* Instance();

	/**
	*Adds a bullet to the player list
	*/
	void AddPlayerBullet(int x, int y, int width, int height, std::string textureID, int numberOfFrames, Vector2D direction);

	/**
	*Adds a bullet to the enemy list
	*/
	void AddEnemyBullet(int x, int y, int width, int height, std::string textureID, int numberOfFrames, Vector2D direction);

	/**
	*Clears both vectors of the class
	*/
	void Clear();

	/**
	*Updates the bullets
	*/
	void Update();

	/**
	*Draws the bullets
	*/
	void Draw();

	std::vector<EnemyBullet*> GetEnemyBullets() { return enemyBullets; }

	std::vector<PlayerBullet*> GetPlayerBullets() { return playerBullets; }

private:
	static BulletHandler* instance;

	std::vector<PlayerBullet*> playerBullets;

	std::vector<EnemyBullet*> enemyBullets;

};
typedef BulletHandler TheBulletHandler;