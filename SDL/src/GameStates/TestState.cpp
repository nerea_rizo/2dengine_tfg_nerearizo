#include "TestState.h"
#include <stdlib.h>

const std::string TestState::stateID = "thousand";


TestState::TestState()
{
}

TestState::TestState(int newLevel)
{
	level = newLevel;
}

TestState::~TestState()
{
}

void TestState::Update()
{
	//ShipCollisions collisions;

	if (!sceneObjects.empty())
	{
		
		int i;
		for (i = 0;i < sceneObjects.size();i++)
		{
			sceneObjects[i]->Update();
		}


	}

	player->Update();
	if (TheInputHandler::Instance()->IsKeyDown(SDL_SCANCODE_ESCAPE))
	{
		TheGame::Instance()->GetGameStateMachine()->PushState(new PauseState());
	}



}


void TestState::Render()
{
	if (!sceneObjects.empty())
	{
		int i = 0;

		for (i = 0; i<sceneObjects.size();i++)
		{
			sceneObjects[i]->Draw();
		}
	}
	/*SDL_Rect* a = new SDL_Rect();
	a->x = player->GetPosition().GetX();	
	a->y= player->GetPosition().GetY();
	a->w = player->GetWidth();
	a->h = player->GetHeight();

	SDL_Rect* b = new SDL_Rect();
	b->x = sceneObjects[0]->GetPosition().GetX();
	b->y = sceneObjects[0]->GetPosition().GetY();
	b->w = sceneObjects[0]->GetWidth();
	b->h = sceneObjects[0]->GetHeight();*/

	/*if (CollisionManager::DrawBoundingBox(a, b, TheGame::Instance()->GetRenderer())) {
		cout << "The objects are colliding" << "\n";
	}
	else
	{
		cout << "No collision detected" << "\n";
	}*/
	player->Draw();

}
bool TestState::OnEnter()
{
	StateParser stateParser;
	stateParser.ParseState("src/GameStates/XMLStates/gameStates.xml", "PlayState1", &backgroundObjects, &textureIDList);

	/*for (int i = 0; i <= 1250; i++)
	{
		BasicEnemyShip* a = new BasicEnemyShip();
		a->Load(new LoaderParams(rand() % 800, rand() % 500, 32, 43, "enemy"));
		sceneObjects.push_back(a);
	}*/

	SDLGameObject* a = new SDLGameObject();
	a->Load(new LoaderParams(rand() % 800, rand() % 500, 32, 43, "enemy"));
	
	sceneObjects.push_back(a);

	player = new Player();
	player->Load(new LoaderParams(10, 300, 32, 43, "player"));



	std::cout << "entering thousand objects state\n";
	return true;
}
bool TestState::OnExit()
{
	
	int i;
	for (i = 0; i < sceneObjects.size(); i++)
	{
		sceneObjects[i]->Clean();
	}
	sceneObjects.clear();

	// clear the texture manager
	for (i = 0; i < textureIDList.size(); i++)
	{
		TheTextureManager::Instance()->ClearFromTextureMap(textureIDList[i]);
	}

	std::cout << "exiting thousand objects state\n";
	return true;
}


void TestState::EliminateLife()
{
	if (playerLives==3)
	{
		playerLives--;
		backgroundObjects.pop_back();
		delete thirdLife;
	}
	else if (playerLives == 2)
	{
		playerLives--;
		backgroundObjects.pop_back();
		delete secondLife;
	}
	else if (playerLives == 1)
	{
		playerLives--;
		backgroundObjects.pop_back();
		delete firstLife;

		exiting = true;

		TheGame::Instance()->GetGameStateMachine()->PopState();
		TheGame::Instance()->GetGameStateMachine()->PushState(new GameOverState());
	}
}

void TestState::OnFinishLevel()
{
	//Mostrar una etiqueta de you win y esperar 180 ticks
	//Si es el primer nivel, cargar el segundo
	//Si es el segundo, volver al men� principal

	if (level == 1)
	{
		TheGame::Instance()->GetGameStateMachine()->PopState();
		TheGame::Instance()->GetGameStateMachine()->PushState(new PlayState(2));
	}
	else if (level == 2)
	{
		TheGame::Instance()->GetGameStateMachine()->PopState();
		TheGame::Instance()->GetGameStateMachine()->PushState(new MainMenuState());
	}

}

