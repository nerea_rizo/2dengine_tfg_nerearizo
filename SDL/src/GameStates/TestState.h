#pragma once
#include "GameState.h"
#include "../Player.h"
#include "../Game.h"
#include "PauseState.h"
#include "GameOverState.h"
#include "../SoundManager.h"
#include "../ShipCollisions.h"

//!Play scene
class TestState :public GameState
{
public:
	virtual void Update();
	virtual void Render();

	//load
	virtual bool OnEnter();

	//clean
	virtual bool OnExit();

	virtual std::string GetStateID() const { return stateID; };

	TestState();
	TestState( int level);
	~TestState();

	//Game Specific 
	int playerLives = 3;
	bool levelCompleted = false;

	/**
	* Charges next level. If there is none, return to the main menu
	*/
	void OnFinishLevel();

	/**
	* substracts a life, eliminates the corresponding life image and if there are no lifes needs charges the Game Over State
	*/
	void EliminateLife();
private:

	static const std::string  stateID;



	int level = 0;

	pair<Vector2D, float> MTV;

	Player* player;

	std::vector<GameObject*> sceneObjects;

	std::vector<GameObject*> backgroundObjects;
	
	SDLGameObject* firstLife;
	SDLGameObject* secondLife;
	SDLGameObject* thirdLife;

};
