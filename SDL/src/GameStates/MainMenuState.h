#pragma once
#include "MenuState.h"
#include "../Game.h"
#include "../Button.h"
#include "StateParser.h"
#include "../SoundManager.h"
//! Main menu scene
class MainMenuState :public MenuState
{
public:
	virtual void Update();
	virtual void Render();

	//load
	virtual bool OnEnter();

	//clean
	virtual bool OnExit();

	virtual std::string GetStateID() const { return stateID; };


	virtual void SetCallbacks(const std::vector<Callback>&callbacks);

	MainMenuState();

	~MainMenuState();

private:
	static void MenuToPlay();
	static void ExitGame();

	static const std::string  stateID;

	std::vector<GameObject*> sceneObjects;

	std::vector<SDLGameObject*> backgroundObjects;
};

