#pragma once
#include "GameState.h"

//! Virtual GameState that prepares callbacks to be set from the XML
class MenuState :public GameState
{
protected:
	typedef void(*Callback)();

	/*
	* Sets a callback to a button depending on the given callbackID
	*/
	virtual void SetCallbacks(const std::vector<Callback>& callbacks)= 0;

	std::vector<Callback> callbacks;
	MenuState() {}
	~MenuState() {}
};

