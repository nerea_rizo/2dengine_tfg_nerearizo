#pragma once
# include <string>
#include <iostream>
#include <vector>
#include "../GameObject.h"

//! Virtual base class for GameStates
class GameState
{
public:
	/**
	* Calls the Update() function of every gameObject on the scene
	*/
	virtual void Update() = 0;
	/**
	* Calls the Draw() function of every gameObject on the scene
	*/
	virtual void Render() = 0;

	/**
	* Parses the scene with the StateParser, sets callbacks and starts music
	*/
	virtual bool OnEnter() = 0;

	/**
	* Deletes every game object on the scene, cleans the vectors and the loaded textures of the scene
	*/
	virtual bool OnExit() = 0;

	virtual std::string GetStateID() const = 0;

	GameState() {};

	~GameState() {};
protected:
	std::vector<std::string> textureIDList;

	bool exiting = false;
};

