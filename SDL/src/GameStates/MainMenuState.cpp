#include "MainMenuState.h"
#include "TestState.h"

const std::string MainMenuState::stateID = "MENU";


MainMenuState::MainMenuState()
{
}


MainMenuState::~MainMenuState()
{
}

void MainMenuState::Update()
{
	if (!sceneObjects.empty() || !exiting)
	{
		int i = 0;
		for (i = 0; i< 2;i++)
		{
			//Si el fondo ha desaparecido completamente
			if (backgroundObjects[i]->GetPosition().GetX() < backgroundObjects[i]->GetWidth()*(-1))
			{
				if (i == 0)
				{
					backgroundObjects[0]->SetPosition(Vector2D(backgroundObjects[1]->GetPosition().GetX() + backgroundObjects[i]->GetWidth(), backgroundObjects[i]->GetPosition().GetY()));
				}
				if (i == 1)
				{
					backgroundObjects[1]->SetPosition(Vector2D(backgroundObjects[0]->GetPosition().GetX() + backgroundObjects[i]->GetWidth(), backgroundObjects[i]->GetPosition().GetY()));
				}
			}

			backgroundObjects[i]->SetPosition(Vector2D(backgroundObjects[i]->GetPosition().GetX() - 1, backgroundObjects[i]->GetPosition().GetY()));
		}
	}

	if (!sceneObjects.empty() || !exiting)
	{
		for (int i = 0; i < sceneObjects.size();i++)
		{
			sceneObjects[i]->Update();
		}
	}
}
void MainMenuState::Render()
{
	if (!sceneObjects.empty() || !exiting)
	{
		int i = 0;
		for (i = 0; i< backgroundObjects.size();i++)
		{
			backgroundObjects[i]->Draw();
		}

		for (i= 0; i < sceneObjects.size();i++)
		{
			sceneObjects[i]->Draw();
		}
	}
}
bool MainMenuState::OnEnter()
{

	StateParser stateParser;
	stateParser.ParseState("src/GameStates/XMLStates/gameStates.xml", MainMenuState::stateID, &sceneObjects, &textureIDList);

	SoundManager::Instance()->PlayMusic("menuMusic",2);

	//push the callbacks so they can be set in the near future
	callbacks.push_back(0); //pushback 0 callbackID start from 1
	callbacks.push_back(MenuToPlay);
	callbacks.push_back(ExitGame);

	SetCallbacks(callbacks);

	SDLGameObject* firstBg = new SDLGameObject();
	firstBg->Load(new LoaderParams(0, 0, 640, 480, "blueClouds"));

	SDLGameObject* secondBg = new SDLGameObject();
	secondBg->Load(new LoaderParams(640, 0, 640, 480, "blueClouds"));

	backgroundObjects.push_back(firstBg);

	backgroundObjects.push_back(secondBg);

	//SoundManager::Instance()->Load("resources/music/Bossa-nova-game-show-thinking-music.mp3", "bossa", SOUND_MUSIC);
	//SoundManager::Instance()->PlayMusic("bossa",0);

	std::cout << "entering MenuState\n";

	return true;

}
bool MainMenuState::OnExit()
{
	int i;
	for (i = 0; i < sceneObjects.size();i++)
	{
		sceneObjects[i]->Clean();
	}
	sceneObjects.clear();

	for (i = 0; i< backgroundObjects.size();i++)
	{
		backgroundObjects[i]->Clean();
	}
	backgroundObjects.clear();

	// clear the texture manager
	for (i = 0; i < textureIDList.size(); i++)
	{
		TheTextureManager::Instance()->	ClearFromTextureMap(textureIDList[i]);
	}

	exiting = true;

	SoundManager::Instance()->StopMusic();

	std::cout << "exiting MenuState\n";
	return true;
}

void MainMenuState::MenuToPlay()
{
	TheGame::Instance()->GetGameStateMachine()->PopState();
	TheGame::Instance()->GetGameStateMachine()->PushState(new PlayState(1));
}

void MainMenuState::ExitGame()
{
	TheGame::Instance()->Quit();
}



void MainMenuState::SetCallbacks(const std::vector<Callback>& callbacks)
{
	for (int i=0 ; i<sceneObjects.size(); i++)
	{
		// if a gameObject is of type Button then assign callback based on the id passed from the file
		if (dynamic_cast<Button*>(sceneObjects[i]))
		{
			Button* button = dynamic_cast <Button*> (sceneObjects[i]);
			button->SetCallback(callbacks[button->GetCallbackID()]);

		}
	}
}
