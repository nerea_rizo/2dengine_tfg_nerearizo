#pragma once
#include "GameState.h"
#include "..\Button.h"
#include "..\Game.h"
//! Pause scene
class PauseState :
	public MenuState
{
public:

	virtual void Update();
	virtual void Render();

	//load
	virtual bool OnEnter();

	//clean
	virtual bool OnExit();

	virtual std::string GetStateID() const { return stateID; };


	virtual void SetCallbacks(const std::vector<Callback>&callbacks);

	PauseState();
	~PauseState();

private:

	static const std::string  stateID;

	std::vector<GameObject*> sceneObjects;

	std::vector<SDLGameObject*> backgroundObjects;

	static void ReturnToTheGame();
	static void ReturnToMainMenu();

};

