#include "GameStateMachine.h"

GameStateMachine::GameStateMachine()
{
}


GameStateMachine::~GameStateMachine()
{
}

void GameStateMachine::Render()
{
	if (!gameStates.empty())
	{
#line 19 "Rendering a gameState from GameStateMachine"
		gameStates.back()->Render();

	}
	else 
	{
		std::cout << "Trying to use Render an empty GameStateList \n";
	}
}

void GameStateMachine::Update()
{
	if (!gameStates.empty())
	{
#line 33 "Updating a gameState from GameStateMachine"
		gameStates.back()->Update();

	}
	else
	{
		std::cout << "Trying to use Update an empty GameStateList \n";
	}
}

void GameStateMachine::PushState(GameState * state)
{
	gameStates.push_back(state);
	gameStates.back()->OnEnter();

}

void GameStateMachine::ChangeState(GameState * state)
{

	if (!gameStates.empty()) 
	{
		//si intentamos meter el estado actual, no hacemos nada
		if (gameStates.back()->GetStateID() == state->GetStateID())
			return;

		//Quitamos el ultimo estado
		if (gameStates.back()->OnExit()) 
		{
			delete gameStates.back();
			gameStates.pop_back();
		}
	}
	else
	{
		std::cout << "the GameState list is empty \n";
	}

	//A�adimos el nuevo estado
	gameStates.push_back(state);
	gameStates.back()->OnEnter();

}

void GameStateMachine::PopState()
{


	if (!gameStates.empty()) 
	{
		if (gameStates.back()->OnExit())
		{
			delete gameStates.back();
			gameStates.pop_back();
		}
	}

}

const void GameStateMachine::PrintStateList() 
{
	if (!gameStates.empty()) 
	{
		std::cout << "GameState List: ";
		for (int i = 0; i < gameStates.size();i++)
		{
			std::cout << gameStates[i]->GetStateID() << " ";
		}
		std::cout << "\n";
	}

}