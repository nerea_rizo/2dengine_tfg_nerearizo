#pragma once
#include <iostream>
#include <vector>
#include <fstream>
#include <cstdio>
#include "../external/tinyxml2.h"
#include "SDL.h"
#include "../Game.h"
#include "../TextureManager.h"
#include "../Factory/GameObjectFactory.h"

#ifndef XMLCheckResult
#define XMLCheckResult(a_eResult) if (a_eResult != tinyxml2::XML_SUCCESS) { printf("Error: %i\n", a_eResult); return a_eResult; }
#endif

//using namespace tinyxml2;

class GameObject;
//! Using TinyXML parses an XML, creates gameObjects and loads textures
class StateParser
{
public:
	/*
	*Parses the objective GameState and sends the base node of the game objects and the textures to ParseObjects() and ParseTextures().
	*If there is an error while reading an error code will be returned.
	*/
	bool ParseState(const char* stateFile, std::string stateID,	std::vector<GameObject*>* Objects, std::vector<std::string> * textureIDs);

	/*
	* Parses the preload.xml document and loads all the window values needed to start the engine.
	*/
	bool ReadInitialValues(std::string &windowName, int &windowXPos, int &windowYPos, int &windowWidth, int &windowHeight, bool &fullscreen, bool &landscape);

private:
	/*
	*Iterates through all the game objects in an XML state and grabs all of their information.
	*After that, it creates the game object in the class stated in the �type� attribute and loads all the information into the Game Object.
	*/
	void ParseObjects(tinyxml2::XMLElement * StateRoot,	std::vector<GameObject*> * Objects);

	/*
	*Iterates through all the textures in an XML state and stores its attributeID to the vector of texture IDs passed from the scene. 
	*After that, the Texture Manager method Load() is called. 
	*/
	void ParseTextures(tinyxml2::XMLElement* StateRoot,	std::vector<std::string> * TextureIDs);

	/*
	*Outputs a string with all the nodes read in order. Its objective is to check if the whole file is being read correctly.
	*/
	void StateParser::GenXmlString(tinyxml2::XMLElement *element, std::string &str);
};

