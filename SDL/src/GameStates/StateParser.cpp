#include "StateParser.h"

bool StateParser::ParseState(const char *stateFile, std::string stateID, std::vector<GameObject*> * gameObjects, std::vector<std::string> * textureIDs)
{
	tinyxml2::XMLDocument doc;

	tinyxml2::XMLError loadResult = doc.LoadFile(stateFile);
	XMLCheckResult(loadResult);

	tinyxml2::XMLNode * firstNode = doc.FirstChild();

	if (firstNode == nullptr) return tinyxml2::XML_ERROR_FILE_READ_ERROR;
	
	// pre declare the states root node
	tinyxml2::XMLElement* stateRoot = 0;
	// get this states root node and assign it to stateRoot
	for (tinyxml2::XMLElement* e = firstNode->FirstChildElement(); e != NULL; e =	e->NextSiblingElement())
	{
		if ( stateID== e->Value())
		{
			stateRoot = e;
		}
	}

	//Textures

	tinyxml2::XMLElement* textureRoot = 0;

	for (tinyxml2::XMLElement* e = stateRoot->FirstChildElement(); e != NULL; e = e->NextSiblingElement())
	{
		if (e->Value() == std::string("TEXTURES"))
		{
			textureRoot= e;
		}
	}


	// now parse the textures
	ParseTextures(textureRoot, textureIDs);

	//Objects

	tinyxml2::XMLElement* objectRoot = 0;
	for (tinyxml2::XMLElement* e = stateRoot->FirstChildElement(); e != NULL; e = e->NextSiblingElement())
	{
		if (e->Value() == std::string("OBJECTS"))
		{
			objectRoot = e;
		}
	}

	ParseObjects(objectRoot, gameObjects);
	return true;

}

bool StateParser::ReadInitialValues(std::string & windowName, int &windowXPos, int & windowYPos, int & windowWidth, int & windowHeight, bool & fullscreen, bool & landscape)
{
	tinyxml2::XMLDocument doc;

	tinyxml2::XMLError loadResult = doc.LoadFile("Preload.xml");

	XMLCheckResult(loadResult);

	tinyxml2::XMLNode * firstNode = doc.FirstChild();

	if (firstNode == nullptr) return tinyxml2::XML_ERROR_FILE_READ_ERROR;

	windowName	 = firstNode->FirstChildElement("windowName")->GetText();

	loadResult=	firstNode->FirstChildElement("xPos")->QueryIntText(&windowXPos);

	if (loadResult != tinyxml2::XML_SUCCESS)
	{
		std::cout << "Error: " << loadResult << "\n";
		return false;
	}


	loadResult=	firstNode->FirstChildElement("yPos")->QueryIntText(&windowYPos);

	if (loadResult != tinyxml2::XML_SUCCESS)
	{
		std::cout << "Error: " << loadResult << "\n";
		return false;
	}


	loadResult=	firstNode->FirstChildElement("width")->QueryIntText(&windowWidth);

	if (loadResult != tinyxml2::XML_SUCCESS)
	{
		std::cout << "Error: " << loadResult << "\n";
		return false;
	}


	loadResult=	firstNode->FirstChildElement("height")->QueryIntText(&windowHeight);

	if (loadResult != tinyxml2::XML_SUCCESS)
	{
		std::cout << "Error: "<< loadResult << "\n";
		return false;
	}
	
	loadResult=	firstNode->FirstChildElement("fullscreen")->QueryBoolText(&fullscreen);

	if (loadResult != tinyxml2::XML_SUCCESS)
	{
		std::cout << "Error: " << loadResult << "\n"; 
		return false;
	}


	loadResult = firstNode->FirstChildElement("landscape") ->QueryBoolText(&landscape);

	if (loadResult != tinyxml2::XML_SUCCESS)
	{
		std::cout << "Error: " << loadResult << "\n";
		return false;
	}
	return true;
}

void StateParser::ParseTextures(tinyxml2::XMLElement * StateRoot, std::vector<std::string>* TextureIDs)
{
	const char* attributeText;

	for (tinyxml2::XMLElement * e = StateRoot->FirstChildElement(); e != NULL;e = e->NextSiblingElement())
	{
		attributeText = e->Attribute("filename");

		if (attributeText == nullptr) std::cout << "Error parsing textures from xml \n";

		std::string fileNameAttribute (attributeText);

		attributeText = e->Attribute("ID");

		if (attributeText == nullptr) std::cout << "Error parsing textures from xml \n";

		std::string idAttribute (attributeText);

		//Everithing went all right, instance the textures
		TextureIDs->push_back(idAttribute); // push into list
		TheTextureManager::Instance()->Load(fileNameAttribute, idAttribute, TheGame::Instance()->GetRenderer());
	}
}

void StateParser::ParseObjects(tinyxml2::XMLElement * StateRoot, std::vector<GameObject*>* objects)
{
	const char* attributeText;
	for (tinyxml2::XMLElement* e = StateRoot->FirstChildElement(); e !=NULL; e = e->NextSiblingElement())
	{
		int x, y, width, height, callbackID, numberOfFrames;
		e->QueryAttribute("x", &x);
		e->QueryAttribute("y", &y);
		e->QueryAttribute("width", &width);
		e->QueryAttribute("height", &height);
		e->QueryAttribute("callbackID", &callbackID);
		e->QueryAttribute("numberOfFrames", &numberOfFrames);
		attributeText = e->Attribute("textureID");
		std::string textureID (attributeText);
		GameObject* GameObject = TheGameObjectFactory::Instance()->Create(e->Attribute("type"));
		GameObject->Load(new LoaderParams(x, y, width, height, textureID, callbackID, numberOfFrames));
		objects->push_back(GameObject);
	}
}

void StateParser::GenXmlString(tinyxml2::XMLElement *element, std::string &str)
{
	if (element == NULL) {
		std::cout << "GenXMLString received a null XMLElement \n";
		return;
	}

	str.append("<");
	str.append(element->Value());
	str.append("> \n");

	if (element->GetText() != NULL) {
		str.append(element->GetText());
	}

	tinyxml2::XMLElement *childElement = element->FirstChildElement();
	while (childElement != NULL) {
		GenXmlString(childElement, str);
		childElement = childElement->NextSiblingElement();
	}

	str.append("</");
	str.append(element->Value());
	str.append("> \n");
}
