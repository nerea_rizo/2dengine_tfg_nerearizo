#pragma once
#include <vector>
#include <iostream>
#include "GameState.h"
//! The scene manager of the engine
class GameStateMachine
{
public:
	GameStateMachine();
	~GameStateMachine();

	/**
	*Calls the Render() function of the last GameState pushed. An error message pops if you are trying to Render() an empty list.
	*/
	void Render();

	/**
	*Calls the Update() function of the last GameState pushed.An error message pops if you are trying to Update() an empty list.
	*/
	void Update();

	/**
	*Pushes a GameState in the list and calls its OnEnter() function.
	*/
	void PushState(GameState* state);

	/**
	* Checks if the GameState queue is empty and if the last GameState pushed is the one we are trying to push right now.
	*If that is not the case, the OnExit() function of the last gameState on the queue is called and after it finishes the GameState is popped. After that, the new GameState is pushed and its OnEnter() is called.
	*/
	void ChangeState(GameState* state);

	/**
	*Checks if the GameStateMachine is empty. If not, pops the last state on the queue.
	*/
	void PopState();

	/**
	*Using the stateIDs on every GameState, prints on the console all the GameStates in the queue in that moment from first to last.
	*/
	const void PrintStateList();

private:
	std::vector<GameState*>gameStates;
};

