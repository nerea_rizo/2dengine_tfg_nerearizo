#include "PauseState.h"
const std::string PauseState::stateID = "PAUSE";


void PauseState::Update()
{
	if (!sceneObjects.empty() || !exiting)
	{
		int i = 0;
		for (i = 0; i< 2;i++)
		{
			//Si el fondo ha desaparecido completamente
			if (backgroundObjects[i]->GetPosition().GetX() < backgroundObjects[i]->GetWidth()*(-1))
			{
				if (i == 0)
				{
					backgroundObjects[0]->SetPosition(Vector2D(backgroundObjects[1]->GetPosition().GetX() + backgroundObjects[i]->GetWidth(), backgroundObjects[i]->GetPosition().GetY()));
				}
				if (i == 1)
				{
					backgroundObjects[1]->SetPosition(Vector2D(backgroundObjects[0]->GetPosition().GetX() + backgroundObjects[i]->GetWidth(), backgroundObjects[i]->GetPosition().GetY()));
				}
			}

			backgroundObjects[i]->SetPosition(Vector2D(backgroundObjects[i]->GetPosition().GetX() - 1, backgroundObjects[i]->GetPosition().GetY()));
		}
	}

	if (!sceneObjects.empty() || !exiting)
	{
		int i = 0;
		for (i = 0;i < sceneObjects.size();i++)
		{
			sceneObjects[i]->Update();
		}
	}
}

void PauseState::Render()
{
	if (!sceneObjects.empty() || !exiting)
	{
		int i = 0;
		for (i = 0; i< backgroundObjects.size();i++)
		{
			backgroundObjects[i]->Draw();
		}
		
		for (i = 0; i < sceneObjects.size();i++)
		{
			sceneObjects[i]->Draw();
		}
	}
}

bool PauseState::OnEnter()
{

	StateParser stateParser;
	stateParser.ParseState("src/GameStates/XMLStates/gameStates.xml", PauseState::stateID, &sceneObjects, &textureIDList);

	//push the callbacks so they can be set in the near future
	callbacks.push_back(0); //pushback 0 callbackID start from 1
	callbacks.push_back(ReturnToTheGame);
	callbacks.push_back(ReturnToMainMenu);

	SetCallbacks(callbacks);

	SDLGameObject* firstBg = new SDLGameObject();
	firstBg->Load(new LoaderParams(0, 0, 640, 480, "blueClouds"));

	SDLGameObject* secondBg = new SDLGameObject();
	secondBg->Load(new LoaderParams(640, 0, 640, 480, "blueClouds"));

	backgroundObjects.push_back(firstBg);

	backgroundObjects.push_back(secondBg);


	std::cout << "entering Pause Mode\n";
	return true;
}

bool PauseState::OnExit()
{
	for (int i = 0; i < sceneObjects.size(); i++)
	{
		sceneObjects[i]->Clean();
	}
	sceneObjects.clear();

	// clear the texture manager
	for (int i = 0; i < textureIDList.size(); i++)
	{
		TheTextureManager::Instance()->ClearFromTextureMap(textureIDList[i]);
	}

	std::cout << "exiting PauseMode \n";

	TheInputHandler::Instance()->OnMouseButtonsReset();

	exiting = true;

	return true;
}


PauseState::PauseState()
{

}


PauseState::~PauseState()
{

}

void PauseState::ReturnToTheGame()
{
	TheGame::Instance()->GetGameStateMachine()->PopState();
}

void PauseState::ReturnToMainMenu()
{
	TheGame::Instance()->GetGameStateMachine()->PopState();
	TheGame::Instance()->GetGameStateMachine()->ChangeState(new MainMenuState());
}


void PauseState::SetCallbacks(const std::vector<Callback>& callbacks)
{
	for (int i = 0; i<sceneObjects.size(); i++)
	{
		// if a gameObject is of type Button then assign callback based on the id passed from the file
		if (dynamic_cast<Button*>(sceneObjects[i]))
		{
			Button* button = dynamic_cast <Button*> (sceneObjects[i]);
			button->SetCallback(callbacks[button->GetCallbackID()]);

		}
	}
}

