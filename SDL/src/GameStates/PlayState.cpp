#include "PlayState.h"

const std::string PlayState::stateID = "PlayState";


PlayState::PlayState()
{
}

PlayState::PlayState(int newLevel)
{
	level = newLevel;
}

PlayState::~PlayState()
{
}

void PlayState::Update()
{
	ShipCollisions collisions;

	if (!sceneObjects.empty())
	{
		int i = 0;
		for (i = 0; i< 2;i++)
		{
			//Si el fondo ha desaparecido completamente
			if (backgroundObjects[i]->GetPosition().GetX() < backgroundObjects[i]->GetWidth()*(-1))
			{
				if (i==0)
				{
					backgroundObjects[0]->SetPosition(Vector2D(backgroundObjects[1]->GetPosition().GetX() + backgroundObjects[i]->GetWidth(), backgroundObjects[i]->GetPosition().GetY()));
				}
				if (i==1)
				{
					backgroundObjects[1]->SetPosition(Vector2D(backgroundObjects[0]->GetPosition().GetX() + backgroundObjects[i]->GetWidth(), backgroundObjects[i]->GetPosition().GetY()));
				}
			}

			backgroundObjects[i]->SetPosition(Vector2D(backgroundObjects[i]->GetPosition().GetX() - 1, backgroundObjects[i]->GetPosition().GetY()));
		}

		for (i = 0;i < sceneObjects.size();i++)
		{
			sceneObjects[i]->Update();
		}


		player->Update();
	}

	if (TheInputHandler::Instance()->IsKeyDown(SDL_SCANCODE_ESCAPE))
	{
		TheGame::Instance()->GetGameStateMachine()->PushState(new PauseState());
	}

	if (TheInputHandler::Instance()->IsKeyDown (SDL_SCANCODE_BACKSPACE))
	{
		EliminateLife();
	}

	int deadEnemies = 0;
	for (int i =0; i < sceneObjects.size();i++)
	{
		if (!sceneObjects[i]->IsDead())
		{
			break;
		}
		else
		{
			deadEnemies++;
			if (deadEnemies == sceneObjects.size()) 
			{
				levelCompleted = true;
			}
		}

	}

	if (!exiting)
	{
		TheBulletHandler::Instance()->Update();

		collisions.CheckEnemyPlayerBulletCollision(sceneObjects);

		collisions.CheckPlayerEnemyBulletCollision(player);

		collisions.CheckPlayerEnemyCollision(player, sceneObjects);
	}
}


void PlayState::Render()
{
	if (!sceneObjects.empty())
	{
		int i = 0;
		for (i =0; i< backgroundObjects.size();i++)
		{
			backgroundObjects[i]->Draw();
		}

		for (i = 0; i<sceneObjects.size();i++)
		{
			sceneObjects[i]->Draw();
		}
	}

	player->Draw();

	if (!exiting)
	{
		TheBulletHandler::Instance()->Draw();
	}
}
bool PlayState::OnEnter()
{
	StateParser stateParser;
	if (level == 1) 
	{
		stateParser.ParseState("src/GameStates/XMLStates/gameStates.xml", PlayState::stateID + "1", &sceneObjects, &textureIDList);
		SoundManager::Instance()->PlayMusic("level1Music",2);
	}
	else if (level==2)
	{
		stateParser.ParseState("src/GameStates/XMLStates/gameStates.xml", PlayState::stateID + "2" , &sceneObjects, &textureIDList);
		SoundManager::Instance()->PlayMusic("level2Music", 2);
	}

	PrepareLevel();

	std::cout << "entering PlayState\n";
	return true;
}
bool PlayState::OnExit()
{
	SoundManager::Instance()->StopMusic();
	int i;
	for (i = 0; i < sceneObjects.size(); i++)
	{
		sceneObjects[i]->Clean();
	}
	sceneObjects.clear();

	for (i = 0; i< backgroundObjects.size();i++)
	{
		backgroundObjects[i]->Clean();
	}
	backgroundObjects.clear();

	// clear the texture manager
	for (i = 0; i < textureIDList.size(); i++)
	{
		TheTextureManager::Instance()->ClearFromTextureMap(textureIDList[i]);
	}

	TheBulletHandler::Instance()->Clear();

	std::cout << "exiting PlayState\n";
	return true;
}


void PlayState::EliminateLife()
{
	if (playerLives==3)
	{
		playerLives--;
		backgroundObjects.pop_back();
		delete thirdLife;
	}
	else if (playerLives == 2)
	{
		playerLives--;
		backgroundObjects.pop_back();
		delete secondLife;
	}
	else if (playerLives == 1)
	{
		playerLives--;
		backgroundObjects.pop_back();
		delete firstLife;

		exiting = true;

		TheGame::Instance()->GetGameStateMachine()->PopState();
		TheGame::Instance()->GetGameStateMachine()->PushState(new GameOverState());
	}
}

void PlayState::OnFinishLevel()
{
	//Mostrar una etiqueta de you win y esperar 180 ticks
	//Si es el primer nivel, cargar el segundo
	//Si es el segundo, volver al men� principal

	if (level == 1)
	{
		TheGame::Instance()->GetGameStateMachine()->PopState();
		TheGame::Instance()->GetGameStateMachine()->PushState(new PlayState(2));
	}
	else if (level == 2)
	{
		TheGame::Instance()->GetGameStateMachine()->PopState();
		TheGame::Instance()->GetGameStateMachine()->PushState(new MainMenuState());
	}

}

void PlayState::PrepareLevel()
{
	SDLGameObject* firstBg = new SDLGameObject();
	SDLGameObject* secondBg = new SDLGameObject();

	if (level == 1)
	{
		firstBg->Load(new LoaderParams(0, 0, 640, 480, "blueClouds"));
		secondBg->Load(new LoaderParams(640, 0, 640, 480, "blueClouds"));
	}
	else 
	{
		firstBg->Load(new LoaderParams(0, 0, 640, 480, "redClouds"));
		secondBg->Load(new LoaderParams(640, 0, 640, 480, "redClouds"));
	}

	firstLife = new SDLGameObject();
	firstLife->Load(new LoaderParams(5, 0, 32, 43, "player"));

	int lifeRotation = 120;
	firstLife->SetRotation(lifeRotation);

	secondLife = new SDLGameObject();
	secondLife->Load(new LoaderParams(41, 0, 32, 43, "player"));
	secondLife->SetRotation(lifeRotation);

	thirdLife = new SDLGameObject();
	thirdLife->Load(new LoaderParams(81, 0, 32, 43, "player"));
	thirdLife->SetRotation(lifeRotation);

	backgroundObjects.push_back(firstBg);

	backgroundObjects.push_back(secondBg);

	backgroundObjects.push_back(firstLife);

	backgroundObjects.push_back(secondLife);

	backgroundObjects.push_back(thirdLife);

	player = new PlayerShip();
	player->Load(new LoaderParams(10, 300, 32, 43, "player"));
	player->SetPlayState(this);
}
