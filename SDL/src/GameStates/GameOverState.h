#pragma once
#include "MenuState.h"
#include "../Game.h"
#include "../Button.h"
#include "StateParser.h"
#include "../SoundManager.h"

//! GameOver scene
class GameOverState :public MenuState
{
public:
	virtual void Update();
	virtual void Render();

	//load
	virtual bool OnEnter();

	//clean
	virtual bool OnExit();

	virtual std::string GetStateID() const { return stateID; };


	virtual void SetCallbacks(const std::vector<Callback>&	callbacks);

	GameOverState();
	~GameOverState();
private:
	static void GameOverToMainMenu();
	static void RestartPlay();

	static const std::string  stateID;

	std::vector<GameObject*> sceneObjects;

	std::vector<SDLGameObject*> backgroundObjects;

};

