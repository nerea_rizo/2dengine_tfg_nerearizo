#include "GameOverState.h"

const std::string GameOverState::stateID = "GAMEOVER";

void GameOverState::Update()
{
	if (!sceneObjects.empty() || !exiting)
	{
		int i = 0;
		for (i = 0; i< 2;i++)
		{
			//Si el fondo ha desaparecido completamente
			if (backgroundObjects[i]->GetPosition().GetX() < backgroundObjects[i]->GetWidth()*(-1))
			{
				if (i == 0)
				{
					backgroundObjects[0]->SetPosition(Vector2D(backgroundObjects[1]->GetPosition().GetX() + backgroundObjects[i]->GetWidth(), backgroundObjects[i]->GetPosition().GetY()));
				}
				if (i == 1)
				{
					backgroundObjects[1]->SetPosition(Vector2D(backgroundObjects[0]->GetPosition().GetX() + backgroundObjects[i]->GetWidth(), backgroundObjects[i]->GetPosition().GetY()));
				}
			}

			backgroundObjects[i]->SetPosition(Vector2D(backgroundObjects[i]->GetPosition().GetX() - 1, backgroundObjects[i]->GetPosition().GetY()));
		}
	}

	if (!sceneObjects.empty() || !exiting)
	{
		for (int i = 0; i < sceneObjects.size();i++)
		{
			sceneObjects[i]->Update();
		}
	}
}
void GameOverState::Render()
{
	if (!sceneObjects.empty() || !exiting)
	{
		int i = 0;
		for (i = 0; i< backgroundObjects.size();i++)
		{
			backgroundObjects[i]->Draw();
		}

		for (i = 0; i < sceneObjects.size();i++)
		{
			sceneObjects[i]->Draw();
		}
	}
}
bool GameOverState::OnEnter()
{
	StateParser stateParser;
	stateParser.ParseState("src/GameStates/XMLStates/gameStates.xml", GameOverState::stateID, &sceneObjects, &textureIDList);

	callbacks.push_back(0); //pushback 0 callbackID start from 1
	callbacks.push_back(RestartPlay);
	callbacks.push_back(GameOverToMainMenu);

	SetCallbacks(callbacks);

	SDLGameObject* firstBg = new SDLGameObject();
	firstBg->Load(new LoaderParams(0, 0, 640, 480, "redClouds"));

	SDLGameObject* secondBg = new SDLGameObject();
	secondBg->Load(new LoaderParams(640, 0, 640, 480, "redClouds"));

	backgroundObjects.push_back(firstBg);

	backgroundObjects.push_back(secondBg);


	std::cout << "entering GameOverState\n";

	return true;

}
bool GameOverState::OnExit()
{
	int i;

	for (i = 0; i < sceneObjects.size();i++)
	{
		sceneObjects[i]->Clean();
	}

	sceneObjects.clear();

	// clear the texture manager
	for (int i = 0; i < textureIDList.size(); i++)
	{
		TheTextureManager::Instance()->ClearFromTextureMap(textureIDList[i]);
	}

	exiting = true;

	std::cout << "exiting GameOverState\n";
	return true;
}

void GameOverState::GameOverToMainMenu()
{
	TheGame::Instance()->GetGameStateMachine()->ChangeState(new	MainMenuState());
}

void GameOverState::RestartPlay()
{
	TheGame::Instance()->GetGameStateMachine()->ChangeState(new	PlayState(1));
}



void GameOverState::SetCallbacks(const std::vector<Callback>& callbacks)
{
	for (int i = 0; i<sceneObjects.size(); i++)
	{
		// if a gameObject is of type Button then assign callback based on the id passed from the file
		if (dynamic_cast<Button*>(sceneObjects[i]))
		{
			Button* button = dynamic_cast <Button*> (sceneObjects[i]);
			button->SetCallback(callbacks[button->GetCallbackID()]);

		}
	}
}

GameOverState::GameOverState()
{
}


GameOverState::~GameOverState()
{
}
