#include "TextureManager.h"

TextureManager* TextureManager::instance = 0;

TextureManager::TextureManager(){}


TextureManager * TextureManager::Instance()
{
	if (instance == 0)
	{
		instance = new TextureManager();
		return instance;
	}

	return instance;
}


bool TextureManager::Load(std::string fileName, std::string	id, SDL_Renderer* pRenderer) 
{
	SDL_Surface* tempSurface = IMG_Load(fileName.c_str());

	//Couldn't create the texture, failure
	if (tempSurface == 0) 
	{
		return false;
	}

	SDL_Texture* pTexture =SDL_CreateTextureFromSurface(pRenderer, tempSurface);

	SDL_FreeSurface(tempSurface);

	// everything went ok, add the texture to our list
	if (pTexture != 0)
	{
		textureMap[id] = pTexture;
		return true;
	}
	// reaching here means something went wrong
	return false;
}

//Draw a sprite
void TextureManager::Draw(std::string id, int x, int y, int	width, int height, SDL_Renderer* pRenderer, SDL_RendererFlip flip, double rotation)
{
	SDL_Rect srcRect;
	SDL_Rect destRect;

	srcRect.x = 0;
	srcRect.y = 0;

	srcRect.w = destRect.w = width;
	srcRect.h = destRect.h = height;

	//Remember, this x and y are the position of were the actual sprite will be drawn in the scene
	destRect.x = x;
	destRect.y = y;

	SDL_RenderCopyEx(pRenderer, textureMap[id], &srcRect, &destRect, rotation, 0, flip);
}

//Draw a frame of an animation
void TextureManager::DrawFrame(std::string id, int x, int y, int width, int height, int currentRow, int currentFrame, SDL_Renderer *pRenderer, SDL_RendererFlip flip, double rotation, int alpha)
{
	SDL_Rect srcRect;
	SDL_Rect destRect;

	srcRect.x = width * currentFrame;
	srcRect.y = height * (currentRow - 1);

	srcRect.w = destRect.w = width;
	srcRect.h = destRect.h = height;

	destRect.x = x;
	destRect.y = y;

	SDL_SetTextureAlphaMod(textureMap[id], alpha);
	SDL_RenderCopyEx(pRenderer, textureMap[id], &srcRect,	&destRect, rotation, 0, flip);
}

TextureManager::~TextureManager()
{
}

void TextureManager::ClearFromTextureMap(std::string id) 
{
	textureMap.erase(id);
}