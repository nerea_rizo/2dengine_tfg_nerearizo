#include "CollisionManager.h"

const pair <Vector2D, float>  CollisionManager::collisionFailure = std::make_pair(Vector2D(0,0),0);

bool CollisionManager::BoundingBox(const SDL_Rect*  First, const SDL_Rect*  Second)
{
	int firstHeightBuffer = First->h / buffer;
	int firstWidthBuffer = First->w / buffer;
	int secondHeightBuffer = Second->h / buffer;
	int secondWidthBuffer = Second->w / buffer;

	// if the bottom of First is less than the top of Second - no collision
	if ((First->y + First->h) - firstHeightBuffer <= Second->y + secondHeightBuffer) { return false; }
	// if the top of First is more than the bottom of Second = no collision
	if (First->y + firstHeightBuffer >= (Second->y + Second->h) - secondHeightBuffer) { return false; }
	// if the right of First is less than the left of Second - no collision
	if ((First->x + First->w) - firstWidthBuffer <= Second->x + secondWidthBuffer) { return false; }
	// if the left of First is more than the right of Second - no collision
	if (First->x + firstWidthBuffer >= (Second->x + Second->w) - secondWidthBuffer) { return false; }
	// otherwise there has been a collision
	return true;
}

pair<Vector2D,float> CollisionManager::SeparatingAxisTheoremWithMTV(GameObject * first, GameObject * second)
{
	double finalOverlap = INFINITY;
	Vector2D smallestAxis = Vector2D();

	VECTOR2D_ARRAY firstVertexList		= CalculateRotatedPointsOfACube(first);
	VECTOR2D_ARRAY SecondVertexList		= CalculateRotatedPointsOfACube(second);

	VECTOR2D_ARRAY firstAxes			= CalculateSeparatingAxes(firstVertexList[TOP_LEFT], firstVertexList[TOP_RIGHT], firstVertexList[BOTTOM_LEFT], firstVertexList[BOTTOM_RIGHT]);
	VECTOR2D_ARRAY secondAxes			= CalculateSeparatingAxes(SecondVertexList[TOP_LEFT], SecondVertexList[TOP_RIGHT], SecondVertexList[BOTTOM_LEFT], SecondVertexList[BOTTOM_RIGHT]);
	
	//Loop over the axes of the first object
	int i;
	for (i =0 ; i< firstAxes.size(); i++)
	{
		Vector2D axis = firstAxes[i];

		//Project both Shapes onto the axis
		Vector2D p1 = ProjectOnAxis(axis, firstVertexList);
		Vector2D p2 = ProjectOnAxis(axis, SecondVertexList);
		if (!AreOverlapping(p1, p2)) 
		{
			//if one fails we know they don't overlap
			return collisionFailure;
		}
		else 
		{
			//calculate Minimum Translation Vector (MTV)
			double lastOverlap = GetOverlap(p1,p2);

			if (lastOverlap< finalOverlap)
			{
				finalOverlap = lastOverlap;
				smallestAxis = axis;
			}
		}
	}

	for (i = 0; i < secondAxes.size(); i++)
	{
		Vector2D axis = secondAxes[i];

		//Project both Shapes onto the axis
		Vector2D p1 = ProjectOnAxis(axis, firstVertexList);
		Vector2D p2 = ProjectOnAxis(axis, SecondVertexList);
		if (!AreOverlapping(p1, p2))
		{
			return collisionFailure;
		}
		else
		{
			//calculate Minimum Translation Vector (MTV)
			double lastOverlap = GetOverlap(p1, p2);

			if (lastOverlap< finalOverlap)
			{
				finalOverlap = lastOverlap;
				smallestAxis = axis;
			}
		}
	}


	return std::make_pair(smallestAxis,finalOverlap);
	
}


bool CollisionManager::SeparatingAxisTheorem(GameObject * first, GameObject* second)
{
	bool intersects = true;
	VECTOR2D_ARRAY firstVertexList = CalculateRotatedPointsOfACube(first);
	VECTOR2D_ARRAY SecondVertexList = CalculateRotatedPointsOfACube(second);

	VECTOR2D_ARRAY firstAxes = CalculateSeparatingAxes(firstVertexList[TOP_LEFT], firstVertexList[TOP_RIGHT], firstVertexList[BOTTOM_LEFT], firstVertexList[BOTTOM_RIGHT]);
	VECTOR2D_ARRAY secondAxes = CalculateSeparatingAxes(SecondVertexList[TOP_LEFT], SecondVertexList[TOP_RIGHT], SecondVertexList[BOTTOM_LEFT], SecondVertexList[BOTTOM_RIGHT]);

	//Loop over the axes of the first object
	int i;
	for (i = 0; i< firstAxes.size(); i++)
	{
		Vector2D axis = firstAxes[i];

		//Project both Shapes onto the axis
		Vector2D p1 = ProjectOnAxis(axis, firstVertexList);
		Vector2D p2 = ProjectOnAxis(axis, SecondVertexList);
		if (!AreOverlapping(p1, p2))
			return false;
	}

	if (intersects)
	{
		for (i = 0; i< secondAxes.size(); i++)
		{
			Vector2D axis = secondAxes[i];

			//Project both Shapes onto the axis
			Vector2D p1 = ProjectOnAxis(axis, firstVertexList);
			Vector2D p2 = ProjectOnAxis(axis, SecondVertexList);
			if (!AreOverlapping(p1, p2))			
				return false;
		}
	}

	return true;

}

VECTOR2D_ARRAY CollisionManager::CalculateSeparatingAxes(GameObject * obj)
{

	 VECTOR2D_ARRAY separatingAxes;

	Vector2D centerOfObj = Vector2D(obj->GetPosition().GetX() + obj->GetWidth() / 2, obj->GetPosition().GetY() + obj->GetHeight()/2);

	Vector2D topLeft = RotatePointAroundAnother(centerOfObj,
												Vector2D(obj->GetPosition().GetX() , obj->GetPosition().GetY()),
												obj->GetRotation());

	Vector2D topRight = RotatePointAroundAnother(centerOfObj,
												Vector2D(obj->GetPosition().GetX() + obj->GetWidth(), obj->GetPosition().GetY()),
												obj->GetRotation());

	Vector2D bottomLeft = RotatePointAroundAnother(centerOfObj,
													Vector2D(obj->GetPosition().GetX() , obj->GetPosition().GetY() + obj->GetHeight()),
													obj->GetRotation());

	Vector2D bottomRight = RotatePointAroundAnother(centerOfObj,
													Vector2D(obj->GetPosition().GetX() + obj->GetWidth(), obj->GetPosition().GetY() + obj->GetHeight()),
													obj->GetRotation());

#pragma region Draw Lines

	//I know that my polygons will always be squares and the transformations will always be calculated from 0,0
	//SDL_SetRenderDrawColor(TheGame::Instance()->GetRenderer(), 255, 255, 255, SDL_ALPHA_OPAQUE);

	//SDL_RenderDrawLine(TheGame::Instance()->getRenderer(), bottomLeft.getX(), bottomLeft.getY(), bottomRight.getX(), bottomRight.getY());
#pragma endregion



	//Calculate edge vectors substracting					// I want the right normal of all the vectors
	Vector2D topEdge	= topLeft		- topRight;			//	<-------
	Vector2D leftEdge	= bottomLeft	- topLeft;			//	|		/
	Vector2D bottomEdge = bottomRight	- bottomLeft;		//	|		|
	Vector2D rightEdge	= topRight		- bottomRight;		//  \------>|
	
	separatingAxes[TOP_EDGE] = topEdge		.rightHandNormal();
	separatingAxes[RIGHT_EDGE] = rightEdge	.rightHandNormal();
	separatingAxes[BOTTOM_EDGE] = leftEdge	.rightHandNormal();
	separatingAxes[LEFT_EDGE] = bottomEdge	.rightHandNormal();

	//We need to normalize the axes to get accurate projections
	separatingAxes[TOP_EDGE].normalize();
	separatingAxes[RIGHT_EDGE].normalize();
	separatingAxes[BOTTOM_EDGE].normalize();
	separatingAxes[LEFT_EDGE].normalize();


	return separatingAxes;
}

VECTOR2D_ARRAY CollisionManager::CalculateSeparatingAxes(Vector2D topLeft, Vector2D topRight, Vector2D bottomLeft, Vector2D bottomRight)
{
	VECTOR2D_ARRAY separatingAxes;

	//Calculate edge vectors substracting					// I want the right normal of all the vectors
	Vector2D topEdge = topLeft - topRight;				//	<-------
	Vector2D leftEdge = bottomLeft - topLeft;			//	|		/
	Vector2D bottomEdge = bottomRight - bottomLeft;		//	|		|
	Vector2D rightEdge = topRight - bottomRight;		//  \------>|

	separatingAxes[TOP_EDGE] = topEdge.rightHandNormal();
	separatingAxes[RIGHT_EDGE] = rightEdge.rightHandNormal();
	separatingAxes[BOTTOM_EDGE] = leftEdge.rightHandNormal();
	separatingAxes[LEFT_EDGE] = bottomEdge.rightHandNormal();

	//We need to normalize the axes to get accurate projections
	separatingAxes[TOP_EDGE].normalize();
	separatingAxes[RIGHT_EDGE].normalize();
	separatingAxes[BOTTOM_EDGE].normalize();
	separatingAxes[LEFT_EDGE].normalize();

	return separatingAxes;
}

VECTOR2D_ARRAY CollisionManager::CalculateRotatedPointsOfACube(GameObject * obj)
{
	Vector2D centerOfObj = Vector2D(obj->GetPosition().GetX() + obj->GetWidth() / 2, obj->GetPosition().GetY() + obj->GetHeight() / 2);

	Vector2D topLeft = RotatePointAroundAnother(centerOfObj,
		Vector2D(obj->GetPosition().GetX(), obj->GetPosition().GetY()),
		obj->GetRotation());

	Vector2D topRight = RotatePointAroundAnother(centerOfObj,
		Vector2D(obj->GetPosition().GetX() + obj->GetWidth(), obj->GetPosition().GetY()),
		obj->GetRotation());

	Vector2D bottomLeft = RotatePointAroundAnother(centerOfObj,
		Vector2D(obj->GetPosition().GetX(), obj->GetPosition().GetY() + obj->GetHeight()),
		obj->GetRotation());

	Vector2D bottomRight = RotatePointAroundAnother(centerOfObj,
		Vector2D(obj->GetPosition().GetX() + obj->GetWidth(), obj->GetPosition().GetY() + obj->GetHeight()),
		obj->GetRotation());

		VECTOR2D_ARRAY rotatedPoints = {topLeft,topRight,bottomLeft,bottomRight};

	return rotatedPoints;
}

Vector2D CollisionManager::RotatePointAroundAnother(Vector2D baseOfRotation, Vector2D pointToBeMoved, float rotation)
{
	//sin y cos de C++ utilizan radianes
	float radRotation = rotation * (PI / 180);

	//Ponemos el punto a trasladar respecto al origen de coordenadas
	Vector2D translatedPoint = pointToBeMoved + (baseOfRotation * -1);

	//Rotamos en funcion del origen de coordenadas
	//Formula:	x'= x cos0 - y sin 0 
	//			y'= x sin0 + y cos 0

	Vector2D rotated = Vector2D(translatedPoint.GetX() * cos(radRotation) - translatedPoint.GetY() * sin(radRotation),
								translatedPoint.GetX() * sin(radRotation) + translatedPoint.GetY() * cos(radRotation));

	Vector2D result = Vector2D(rotated + baseOfRotation);

	//Quitamos la translación que hemos hecho para rotar respecto al 0,0
	return result;
}

Vector2D CollisionManager::ProjectOnAxis(Vector2D axis, VECTOR2D_ARRAY vertexList)
{
	//Como el eje es unitario, solo multiplicamos el eje por le vertice. Si no seria asi.
	//Formula:	projection.x = ( dp / (b.x*b.x + b.y*b.y) ) * b.x;
	//			projection.y = (dp / (b.x*b.x + b.y*b.y)) * b.y;

	double min = dotProduct(axis, vertexList[0]);
	double max = min;

	int i;
	for (i=1; i< vertexList.size(); i++)
	{
		double projection = dotProduct(axis, vertexList[i]);

		if (projection < min)
		{
			min = projection;
		}
		else if (projection >max) 
		{
			max = projection;
		}
	}

	return Vector2D(min, max);
}

bool CollisionManager::AreOverlapping(Vector2D firstProj, Vector2D secondProj)
{
	return firstProj.GetY() > secondProj.GetX()  && firstProj.GetX() < secondProj.GetY();
}

float CollisionManager::GetOverlap(Vector2D firstProjection, Vector2D secondProjection)
{
	if (AreOverlapping(firstProjection, secondProjection)) 
	{
		return fmin(firstProjection.GetX(), secondProjection.GetX()) - fmax(firstProjection.GetY(), secondProjection.GetY());
	}
}


CollisionManager::CollisionManager()
{
}


CollisionManager::~CollisionManager()
{
}

bool CollisionManager::DrawSeparatingAxisTheorem(GameObject * first, GameObject * second, SDL_Renderer * renderer)
{
	bool intersects = true;
	SDL_SetRenderDrawColor(renderer, 255, 255, 255, SDL_ALPHA_OPAQUE);

	VECTOR2D_ARRAY firstVertexList = CalculateRotatedPointsOfACube(first);
	VECTOR2D_ARRAY SecondVertexList = CalculateRotatedPointsOfACube(second);

	VECTOR2D_ARRAY firstAxes = CalculateSeparatingAxes(firstVertexList[TOP_LEFT], firstVertexList[TOP_RIGHT], firstVertexList[BOTTOM_LEFT], firstVertexList[BOTTOM_RIGHT]);
	VECTOR2D_ARRAY secondAxes = CalculateSeparatingAxes(SecondVertexList[TOP_LEFT], SecondVertexList[TOP_RIGHT], SecondVertexList[BOTTOM_LEFT], SecondVertexList[BOTTOM_RIGHT]);


	SDL_RenderDrawLine(renderer, SecondVertexList[TOP_LEFT].GetX(), SecondVertexList[TOP_LEFT].GetY(), SecondVertexList[TOP_RIGHT].GetX(), SecondVertexList[TOP_RIGHT].GetY());
	SDL_RenderDrawLine(renderer, SecondVertexList[TOP_RIGHT].GetX(), SecondVertexList[TOP_RIGHT].GetY(), SecondVertexList[BOTTOM_RIGHT].GetX(), SecondVertexList[BOTTOM_RIGHT].GetY());
	SDL_RenderDrawLine(renderer, SecondVertexList[BOTTOM_RIGHT].GetX(), SecondVertexList[BOTTOM_RIGHT].GetY(), SecondVertexList[BOTTOM_LEFT].GetX(), SecondVertexList[BOTTOM_LEFT].GetY());
	SDL_RenderDrawLine(renderer, SecondVertexList[BOTTOM_LEFT].GetX(), SecondVertexList[BOTTOM_LEFT].GetY(), SecondVertexList[TOP_LEFT].GetX(), SecondVertexList[TOP_LEFT].GetY());


	SDL_RenderDrawLine(renderer, firstVertexList[TOP_LEFT].GetX(), firstVertexList[TOP_LEFT].GetY(), firstVertexList[TOP_RIGHT].GetX(), firstVertexList[TOP_RIGHT].GetY());
	SDL_RenderDrawLine(renderer, firstVertexList[TOP_RIGHT].GetX(), firstVertexList[TOP_RIGHT].GetY(), firstVertexList[BOTTOM_RIGHT].GetX(), firstVertexList[BOTTOM_RIGHT].GetY());
	SDL_RenderDrawLine(renderer, firstVertexList[BOTTOM_RIGHT].GetX(), firstVertexList[BOTTOM_RIGHT].GetY(), firstVertexList[BOTTOM_LEFT].GetX(), firstVertexList[BOTTOM_LEFT].GetY());
	SDL_RenderDrawLine(renderer, firstVertexList[BOTTOM_LEFT].GetX(), firstVertexList[BOTTOM_LEFT].GetY(), firstVertexList[TOP_LEFT].GetX(), firstVertexList[TOP_LEFT].GetY());


	//Loop over the axes of the first object
	int i;
	for (i = 0; i< firstAxes.size(); i++)
	{
		Vector2D axis = firstAxes[i];

		//Project both Shapes onto the axis
		Vector2D p1 = ProjectOnAxis(axis, firstVertexList);
		Vector2D p2 = ProjectOnAxis(axis, SecondVertexList);
		if (!AreOverlapping(p1, p2))
			return false;
	}

	if (intersects)
	{
		for (i = 0; i< secondAxes.size(); i++)
		{
			Vector2D axis = secondAxes[i];

			//Project both Shapes onto the axis
			Vector2D p1 = ProjectOnAxis(axis, firstVertexList);
			Vector2D p2 = ProjectOnAxis(axis, SecondVertexList);
			if (!AreOverlapping(p1, p2))
				return false;
		}
	}

	return true;
}

bool CollisionManager::DrawBoundingBox(const SDL_Rect * First, const SDL_Rect * Second, SDL_Renderer * renderer)
{
	int firstHeightBuffer = First->h / buffer;
	int firstWidthBuffer = First->w / buffer;
	int secondHeightBuffer = Second->h / buffer;
	int secondWidthBuffer = Second->w / buffer;

	SDL_SetRenderDrawColor(renderer, 255, 255, 255, SDL_ALPHA_OPAQUE);

	SDL_RenderDrawLine(renderer, First->x, First->y , First->x + First->w, First->y);
	SDL_RenderDrawLine(renderer, First->x + First->w, First->y, First->x + First->w, First->y +First->h);
	SDL_RenderDrawLine(renderer, First->x + First->w, First->y + First->h, First->x, First->y + First->h);
	SDL_RenderDrawLine(renderer, First->x , First->y + First->h, First->x, First->y );


	SDL_RenderDrawLine(renderer, Second->x, Second->y, Second->x + Second->w, Second->y);
	SDL_RenderDrawLine(renderer, Second->x + Second->w, Second->y, Second->x + Second->w, Second->y + Second->h);
	SDL_RenderDrawLine(renderer, Second->x + Second->w, Second->y + Second->h, Second->x, Second->y + Second->h);
	SDL_RenderDrawLine(renderer, Second->x, Second->y + Second->h, Second->x, Second->y);
	


	// if the bottom of First is less than the top of Second - no collision
	if ((First->y + First->h)  <= Second->y ) { return false; }
	// if the top of First is more than the bottom of Second = no collision
	if (First->y  >= (Second->y + Second->h)) { return false; }
	// if the right of First is less than the left of Second - no collision
	if ((First->x + First->w)<= Second->x ) { return false; }
	// if the left of First is more than the right of Second - no collision
	if (First->x  >= (Second->x + Second->w) ) { return false; }
	// otherwise there has been a collision
	return true;
}
