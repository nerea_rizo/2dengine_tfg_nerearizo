#pragma once
#include "SDLGameObject.h"

//! Basic class for the Shoot'em up game
class Ship :public SDLGameObject
{
public:
	~Ship();

	virtual void Draw();
	virtual void Update();
	virtual void Clean() {};

	virtual void Load(const LoaderParams* params);

	virtual void OnCollision() {};

protected:

	Ship();
	/*
	* Runs the dying animation using dyingTime and dyingCounter. After finishing dead is set to true
	*/
	void StartDyingAnimation();

	float movementSpeed=1.5f;

	///In frames
	int dyingTime=9;
	///In frames
	int dyingCounter=0;

	int shootingSpeed=350;
	int framesUntilShot=0;

	bool deathSoundPlayed=false;
};


