#include <iostream>
#include <SDL.h>
#include <ctime>
#include "./src/Game.h"
using namespace std;

const int FPS = 60;
const int DELAY_TIME = 1000.0f / FPS;
// our Game object
Game* game = 0;


//These values will be read from the document in the beggining
//e.g Galaxy Tab 1280*800 landscape
////////////////////////////////////
std::string windowName = "SDL Start!";
int windowXPos = 700;
int windowYPos = 100;
int windowWidth = 1440;
int windowHeight = 900;
bool fullscreen = true;
bool landscape = false;
////////////////////////////////////


clock_t current_ticks, delta_ticks;
clock_t fps = 0;


int main(int argc, char* argv[])
{
	//Needed for delta
	Uint32 frameStart, frameTime;
	StateParser stateParser;
	if(!stateParser.ReadInitialValues(	windowName, windowXPos, windowYPos, 
										windowWidth, windowHeight, fullscreen, landscape))
	{
		std::cout << "Reading values from start file failed. Loading Default Values \n";
		std::string windowName = "SDL Start!";
		windowXPos = 700;
		windowYPos = 100;
		windowWidth = 1440;
		windowHeight = 900;
		fullscreen = true;
		landscape = false;
	}

	if (TheGame::Instance()->Init(windowName, windowXPos, windowYPos, windowWidth, windowHeight, fullscreen, landscape))
	{
		while (TheGame::Instance()->Running())
		{
			current_ticks = clock();

			frameStart = SDL_GetTicks();
			TheGame::Instance()->HandleEvents();
			TheGame::Instance()->Update();
			TheGame::Instance()->Draw();

			frameTime = SDL_GetTicks() - frameStart;

			//if the game is going too fast, wait for next frame
			if (frameTime < DELAY_TIME) {
				SDL_Delay((int)(DELAY_TIME - frameTime));
			}

			//To calculate FPS we need to recalculate frameTime
			frameTime = SDL_GetTicks() - frameStart;
			if (frameTime > 0)
				fps = CLOCKS_PER_SEC / frameTime;
			//cout << "Frame rate is:" <<fps << "fps" << endl;


		}
	}
	else 
	{
		std::cout << "game Init failure -> " <<SDL_GetError()<<"\n";
		SDL_Delay(1000);
		return -1;
	}
	TheGame::Instance()->Quit();
	return 0;

}


/*! \mainpage Index Page
*
* \section intro_sec Introduction and personal motivation
*
* This Undergraduate Dissertation has the objective of developing a simple game engine in C++. This engine will be able to run games given an XML file which will contain the information of the scene and all of its components. 
*
*Game engines are the main tool used by video game developers nowadays. They are powerful software capable of handling a large amount of tasks by themselves. This tasks include, for example, physics, audio, camera and collision detection among others. 
*
*As a student I have used different game engines but I never quite understood how they work under the hood. Its logic and way of handling different tasks amazed me and I wanted to know how they worked. I think this knowledge would allow me to develop better games.
*
*I will be doing this Undergraduate Dissertation as my mean of learning more about C++ and game engines. I think that even though it may be challenging the experience obtained from making an engine will prove to be useful in the future and will improve my knowledge of the video game development process and its possible hardships. Working at such low level will provide me insight in fields such as memory management and low-level graphics while refining my encapsulation abilites, efficient code writing and data structures management.
*
*
* \section install_sec Objectives
*
* - Developing an efficient game engine in C++ with capabilities such as game objects,
*collision detection, sprite rendering, input reading, playing sound, reading from an XML and basic vector handling.
*
* - Writing the documentation of the game engine explaining in insight its classes and
*functions.
*
* - The game engine will have good memory allocation and will be properly encapsulated.
*
*/